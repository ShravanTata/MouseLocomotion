# Task List

- [x] Add model documentation
- [ ] Set-up the optimization framework
- [ ] Experiment with muscle activation time
- [ ] Clean up ROS installation 
- [x] Make a to-do list
- [ ] Goal 1 : Make it visually walk with four limbs
- [ ] Add list of fore-limb muscles to the mouse model documentation
