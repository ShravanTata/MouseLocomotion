import sys
sys.path.append('../code')
from ClosedLoopManager import ClosedLoopManager
from tools import structures_tools as tls

def main():
	""" This program executes a neural-biomechanical closed loop simulation.
	The program launches the neural netwrok (python) and the biomechanical
	model (cpp) and manages the communication between these two programs.
	The program doesn't have to be executed by MPI.
	"""
	if len(sys.argv)<4:
		print "Error in arguments. Required arguments:"
		print "\t ees frequency [0-1000] "
		print "\t ees amplitude (0-600] or %Ia_II_Mn "
		print "\t experiment [bed, treadmill, bodyweight] "
		print "Optional arguments:"
		print "\t Species (mouse, rat or human)"
		print "\t tot time of simulation (default = 3000)"
		print "\t figure name (default = '')"

		sys.exit(-1)

	nnNp = "1"
	eesFreq = sys.argv[1]
	eesAmp = sys.argv[2]
	experiment = sys.argv[3]
	if len(sys.argv)>=5: species = sys.argv[4]
	else: species = "human"
	if len(sys.argv)>=6 : totTime = sys.argv[5]
	else: totTime = "3000"
	if len(sys.argv)>=7: figName  = sys.argv[6]
	else: figName  = ""

	w1 = 0.09#0.09
	w2 = 0.03
	w3 = 0.045#0.046
	w4 = -0.00145
	w5 = -0.0045
	templateFile = "templateClosedLoopSimple.txt"
	nnStructFile = "generatedStructures/ClosedLoop_w1_%f_w2_%f_w3_%f_w4_%f_w5_%f.txt" % (w1,w2,w3,w4,w5)

	tls.modify_network_structure(templateFile,nnStructFile,None,[w1,w2,w3,w4,w5])

	clm = ClosedLoopManager(nnNp, eesFreq, eesAmp, nnStructFile, experiment, species, totTime, figName )
	clm.run()

if __name__ == '__main__':
	main()
