import sys
args =  list(sys.argv)
sys.path.append('../code')
from mpi4py import MPI
import pyNN.nest as sim
from simulations import ClosedLoop
from SpinalNeuralNetwork import SpinalNeuralNetwork
from EES import EES
import numpy as np

comm = MPI.COMM_WORLD
sizeComm = comm.Get_size()
rank = comm.Get_rank()

def main():
	""" This program runs the neural network for a closed loop simulation.
	MPI is supported.
	"""
	if len(args)<4:
		if rank==0:
			print "Error in arguments. Required arguments:"
			print "\t ees frequency [0-1000] "
			print "\t ees amplitude (0-600] or %Ia_II_Mn "
			print "\t neural network structure file .txt"
			print "Optional arguments:"
			print "Species (mouse, rat or human)"
			print "\t tot time of simulation (default = 999999)"
			print "\t figure name (default = '')"
		sys.exit(-1)

	eesFrequency = float(args[1])

	if args[2][0]=="%": eesAmplitude = [float(x) for x in args[2][1:].split("_")]
	else: eesAmplitude = float(args[2])
	inputFile = args[3]
	if len(args)>=5: species = args[4]
	else: species = "human"
	if len(args)>=6: tStop = float(args[5])
	else: tStop = 999999
	if len(args)>=7: figName  = args[6]
	else: figName  = ""
	print species
	print "\nSetting up simulation environment..."
	extra = {'threads' : 4}
	sim.setup(timestep=0.1, min_delay=0.1, max_delay=2.0,**extra)
	print "\nCreating simulation variables..."
	nn=SpinalNeuralNetwork(inputFile)

	dtCommunication = 20
	ees = EES(nn,eesAmplitude,eesFrequency)
	ees.get_amplitude(True)

	simulation = ClosedLoop(nn, dtCommunication, species, ees, tStop, figName )
	simulation.run()

if __name__ == '__main__':
	main()
