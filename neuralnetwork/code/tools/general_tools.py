from mpi4py import MPI
import numpy as np
import os
import fnmatch
import subprocess

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

def load_txt_mpi(fileName):
	""" Load txt data files from one process and broadcast them to the odther processes.
	This loader is implemented to avoid race conditions.
	"""
	data = None
	if rank == 0: data = np.loadtxt(fileName)
	data = comm.bcast(data,root=0)
	return data

def naive_string_clustering(stringList):
	clusters = []
	for i,string1 in enumerate(stringList):
		clusters.append([])
		clusters[-1].append(string1)
		for j,string2 in enumerate(stringList):
			if i==j:continue
			if len(set(list(string1)).intersection(list(string2)))>=2:
				clusters[-1].append(string2)
	found = []
	for stringList in clusters:
		foundFlag=0
		for foundList in found:
			if set(foundList) == set(stringList):
				foundFlag=1
		if not foundFlag:found.append(stringList)
	return found

def find(pattern, path):
	" Finds the files in a path with a given pattern. "
	result = []
	for root, dirs, files in os.walk(path):
		for name in files:
			if fnmatch.fnmatch(name, pattern):
				result.append(os.path.join(root, name))
	return result

def run_subprocess(program):
	""" Runs a given program as a subrocess. """

	returnCode = None
	while not returnCode==0:
		p = subprocess.Popen(program, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		returnCode = None
		while returnCode is None:
			message =  p.stdout.readline().rstrip("\n").split()
			if message != None:print "\t\t"+" ".join(message)+"\t\t"
			returnCode = p.poll()
		if returnCode != 0: print "\t\t\t\t Error n: ",p.poll()," resetting simulation..."
