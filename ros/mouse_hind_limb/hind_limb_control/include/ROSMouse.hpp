#ifndef __ROSMOUSE_HPP__
#define __ROSMOUSE_HPP__

#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64.h>
#include <rosgraph_msgs/Clock.h>
#include <hind_limb_msgs/muscles.h>

#include <sstream>
#include <sml/sml.hh>
#include <iostream>
#include <string>
#include <sstream>
#include <math.h>
#include <vector>
#include <fstream>
#include <map>					
#include <limits>
#include <sml/types/types.h>
#include <sml/musculoSkeletalSystem/RosMouse.hh>
#include <boost/xpressive/xpressive.hpp>
#include <sml/sml-tools/PerturbationManager.hh>
#include <sml/sml-tools/EventManager.hh>

#define INVERSE_SIMULATION 0
#define SPINAL_SIMULATION 0

class ROSMouse : public RosMouse {

private:
	//Variables
	ros::NodeHandle node; // Node Handle
	int rosCommRate; // ROS Communication Rate
	std::map <std::string,float> jointPosition;
	double time;
	typedef RosMouse sml;
	const int numJoints = 3;
	double muscleActivations[8]; 

	// Publishers
	ros::Publisher joint_pelvis_pub_;
	ros::Publisher joint_hip_pub_;
	ros::Publisher joint_knee_pub_;
	ros::Publisher joint_ankle_pub_;
	ros::Publisher muscle_activation_pub_;

	// Subscribers
	ros::Subscriber joint_state_sub_;
	ros::Subscriber gazebo_clock_sub_;
	ros::Subscriber muscle_activation_sub_;

	// Messages
	std_msgs::Float64 joint_pelvis_msg_;
	std_msgs::Float64 joint_hip_msg_;
	std_msgs::Float64 joint_knee_msg_;
	std_msgs::Float64 joint_ankle_msg_;
	hind_limb_msgs::muscles muscle_activation_msg_;

	//Functions
	void jointStateCallback(const sensor_msgs::JointState &msg);
	void gazeboClockCallback(const rosgraph_msgs::Clock &msg);
	void muscleActivationStateCallback(const hind_limb_msgs::muscles &msg);
    void ConstantInit();
    void InputInit();
    void InputUpdate();
    void step_SPINE_TO_MTU(double dt);
    int step();

public:
	//Variables
    //typedef RosMouse sml;

	// Functions
	ROSMouse(): RosMouse(){
        this->init();
        }; // Constructor
	~ROSMouse(){};// Destructor
	void init();
	void run();

};


#endif