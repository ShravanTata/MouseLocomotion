#include "ROSMouse.hpp"
#include <sml/sml.hh>

using namespace std;

CentralClock * centralclock;
double debug=false;
std::map<std::string, double_int_string> Settings::sets;

extern SmlParameters parameters;

int main(int argc, char **argv)
{
	// Initialise ros
	ros::init(argc, argv, "joint_torque_control");

    cout << "1) Loading settings..............." << endl;

	Settings::filename="settings_pendulum.xml";

	Settings::path="/home/tatarama/catkin_ws/src/mouse_hind_limb/hind_limb_control/conf_sample/settings/";


	Settings settings;

	// Run the node

	ROSMouse *Mickey = new ROSMouse( );

	std::cout << "Initialised joint torque control node successfully " << std::endl;

	Mickey->run();

	std::cout << "Terminating joint torque control node successfully " << std::endl;

	return 0;

}
