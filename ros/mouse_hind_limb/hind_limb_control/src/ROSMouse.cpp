#include "ROSMouse.hpp"
#include <sml/sml-tools/Settings.hh>
#include <cstdlib>
#include <ctime>

using namespace boost::xpressive;
using namespace MOUSE;
extern double debug;
extern EventManager* eventManager;
extern SmlParameters parameters;
using namespace std;

using namespace MOUSE;

//extern double debug;

// Constructor
void ROSMouse::init() {
    cout << "Initialize ROSMouse : " << endl;

    sml::init();

    // ROS Node rate
    rosCommRate = 1000;

    // Initialize Publisher topics
    // joint_pelvis_pub_ = node.advertise<std_msgs::Float64>("/single_hind_limb/base_link_to_Pelvis_position_controller/command", 1);
    joint_hip_pub_ = node.advertise<std_msgs::Float64>("/single_hind_limb/hip_effort_controller/command", 1);
    joint_knee_pub_ = node.advertise<std_msgs::Float64>("/single_hind_limb/knee_effort_controller/command", 1);
    joint_ankle_pub_ = node.advertise<std_msgs::Float64>("/single_hind_limb/ankle_effort_controller/command", 1);

    muscle_activation_pub_ = node.advertise<hind_limb_msgs::muscles>("/single_hind_limb/muscle_activation_pub/",1);

    // Initialize Subscriber topics
    joint_state_sub_ = node.subscribe("/single_hind_limb/joint_states", 1, &ROSMouse::jointStateCallback, this);

    gazebo_clock_sub_ = node.subscribe("/clock", 1, &ROSMouse::gazeboClockCallback, this);

    muscle_activation_sub_ = node.subscribe("/single_hind_limb/muscle_activation", 1 , &ROSMouse::muscleActivationStateCallback, this);

    // Initialize Muscle Activation
    for (unsigned int j = 0; j < 8; j++)
    {
        muscleActivations[j] = 0.01;
    }

}

void ROSMouse::jointStateCallback(const sensor_msgs::JointState &msg) {
// Function for recording joint state angles and velocities

    jointPosition[JOINT::toString(0)] = msg.position[1];
    jointPosition[JOINT::toString(1)] = msg.position[2];
    jointPosition[JOINT::toString(2)] = msg.position[0];

}

void ROSMouse::gazeboClockCallback(const rosgraph_msgs::Clock &msg) {

    time = msg.clock.toSec();

}

void ROSMouse::muscleActivationStateCallback(const hind_limb_msgs::muscles &msg)
{
    muscleActivations[0] = msg.pma;
    muscleActivations[1] = msg.cf;
    muscleActivations[2] = msg.sm;
    muscleActivations[3] = msg.pop;
    muscleActivations[4] = msg.rf;
    muscleActivations[5] = msg.ta;
    muscleActivations[6] = msg.sol;
    muscleActivations[7] = msg.lg;
}


void ROSMouse::run() {

    ros::Rate loop_rate(rosCommRate);

    //
    ConstantInit();
    print_debug("[OK] Constant Init");

    InputInit();
    print_debug("[OK] Input Init");


    while (ros::ok()) {

        this->step();

        loop_rate.sleep();

        // joint_pelvis_pub_.publish(joint_pelvis_msg_);
        joint_hip_pub_.publish(joint_hip_msg_);
        joint_knee_pub_.publish(joint_knee_msg_);
        joint_ankle_pub_.publish(joint_ankle_msg_);

        muscle_activation_pub_.publish(muscle_activation_msg_);

        ros::spinOnce();

    }
}

void ROSMouse::ConstantInit() {
    // Segment constant
    sml::Constant[CONSTANT::LENGTH_THIGH] = 0.016089;

    sml::Constant[CONSTANT::WEIGHT_THIGH] = 0.000441;

    sml::Constant[CONSTANT::LENGTH_SHIN] = 0.017879;

    sml::Constant[CONSTANT::WEIGHT_SHIN] = 0.000216;

    sml::Constant[CONSTANT::LENGTH_FOOT] = 0.016258;

    sml::Constant[CONSTANT::WEIGHT_FOOT] = 0.000063;

    // Joints constant
    sml::Constant[CONSTANT::REF_POS_HIP_LEFT] = 0 * (2.0 * PI / 360.0);

    sml::Constant[CONSTANT::MAX_POS_HIP_LEFT] = +50.0  * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_HIP_LEFT];

    sml::Constant[CONSTANT::MIN_POS_HIP_LEFT] = -30.0  * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_HIP_LEFT];

    sml::Constant[CONSTANT::REF_POS_KNEE_LEFT] = 90 * (2.0 * PI / 360.0);

    sml::Constant[CONSTANT::MAX_POS_KNEE_LEFT] = -45.0  * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];

    sml::Constant[CONSTANT::MIN_POS_KNEE_LEFT] = -145.0  * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];

    sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT] = 0 * (2.0 * PI / 360.0);

    sml::Constant[CONSTANT::MAX_POS_ANKLE_LEFT] = +45.0  * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];

    sml::Constant[CONSTANT::MIN_POS_ANKLE_LEFT] = -45.0  * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];

    //PMA
    sml::Constant[CONSTANT::REF_PHI_PMA] =  7.5758 * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_HIP_LEFT];

    sml::Constant[CONSTANT::MAX_PHI_PMA] =  41.9192 * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_HIP_LEFT];

    //CF
    sml::Constant[CONSTANT::REF_PHI_CF] = 0.5050 * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_HIP_LEFT];

    sml::Constant[CONSTANT::MAX_PHI_CF] = -21.7172 * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_HIP_LEFT];

    //SM
    sml::Constant[CONSTANT::REF_PHI_SM_HIP] = -29.798 * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_HIP_LEFT];

    sml::Constant[CONSTANT::MAX_PHI_SM_HIP] = -4.5455 * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_HIP_LEFT];

    sml::Constant[CONSTANT::REF_PHI_SM_KNEE] = -61.5152 * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];

    sml::Constant[CONSTANT::MAX_PHI_SM_KNEE] = -92.2727 * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];


    //POP
    //CHANGE = -118.6364 -> -90
    sml::Constant[CONSTANT::REF_PHI_POP] =  -95.6364 * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];

    sml::Constant[CONSTANT::MAX_PHI_POP] = -136.2121 * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];


    //RF
    sml::Constant[CONSTANT::REF_PHI_RF] =  -95.404 * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];

    sml::Constant[CONSTANT::MAX_PHI_RF] =  -45.404 * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];


    //TA
    sml::Constant[CONSTANT::REF_PHI_TA] =  22.7273 * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];

    sml::Constant[CONSTANT::MAX_PHI_TA] =  10.6061 * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];


    //SOL
    sml::Constant[CONSTANT::REF_PHI_SOL] =  -2.5253 * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];

    sml::Constant[CONSTANT::MAX_PHI_SOL] =  2.5253 * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];


    //LG
    sml::Constant[CONSTANT::REF_PHI_LG_KNEE] = -38.0808 * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];

    sml::Constant[CONSTANT::MAX_PHI_LG_KNEE] = -45.404 * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];

    sml::Constant[CONSTANT::REF_PHI_LG_ANKLE] = -20.7071 * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];

    sml::Constant[CONSTANT::MAX_PHI_LG_ANKLE] = -0.5050 * (2.0 * PI / 360.0) + sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
}

void ROSMouse::InputInit() {

    InputUpdate();

}

void ROSMouse::InputUpdate() {
    for (unsigned joint_pos = INPUT::FIRST_JOINT, joint = JOINT::FIRST_JOINT; joint_pos <= INPUT::FIRST_JOINT + numJoints; joint_pos++, joint++) {
        sml::Input[joint_pos] = -1 * jointPosition[JOINT::toString(joint)];
    }
}

void ROSMouse::step_SPINE_TO_MTU(double dt) {

    muscle_activation_msg_.pma = muscleActivations[0];
    muscle_activation_msg_.cf = muscleActivations[1];
    muscle_activation_msg_.sm = muscleActivations[2];
    muscle_activation_msg_.pop = muscleActivations[3];
    muscle_activation_msg_.rf = muscleActivations[4];
    muscle_activation_msg_.ta = muscleActivations[5];
    muscle_activation_msg_.sol = muscleActivations[6];
    muscle_activation_msg_.lg = muscleActivations[7];    

    muscles[0]->stim = muscleActivations[0];
    muscles[1]->stim = muscleActivations[1];
    muscles[2]->stim = muscleActivations[2];
    muscles[3]->stim = muscleActivations[3];
    muscles[4]->stim = muscleActivations[4];
    muscles[5]->stim = muscleActivations[5];
    muscles[6]->stim = muscleActivations[6];
    muscles[7]->stim = muscleActivations[7];

}

int ROSMouse::step() {
    //CHECK
    double dt = 0.001;

    sml::step();

    step_SPINE_TO_MTU(dt);

    double torque[numJoints + 1];

    for (
        unsigned joint = JOINT::FIRST_JOINT,
        joint_tau = OUTPUT::FIRST_JOINT;
        joint_tau <= OUTPUT::FIRST_JOINT + numJoints;
        joint_tau++,
        joint++
    )
    {

        torque[joint] = sml::Output[joint_tau];
    }
    
    // joint_pelvis_msg_.data = 0.0;
    joint_hip_msg_.data = torque[0] * -10000000;
    joint_knee_msg_.data = torque[1] * -10000000;
    joint_ankle_msg_.data = torque[2] * -10000000;

    return 0;

}
