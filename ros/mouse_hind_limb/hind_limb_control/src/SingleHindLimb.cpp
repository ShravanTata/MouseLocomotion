#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64.h>
#include <math.h>

int main(int argc, char **argv)
{
	// Initialise ros
	ros::init(argc, argv, "joint_control");

	// Declare the NodeHandle
	ros::NodeHandle node;

	// Declare Joint State Publisher
	ros::Publisher joint_pub = node.advertise<std_msgs::Float64>("/single_hind_limb/Tibia_to_Ankle_effort_conoller/command",100);

	// Declare Ros Rate
	ros::Rate loop_rate(100);

	while(ros::ok()){

		static float time = 0;

		joint_pub.publish(sin(2*3.1428*time*0.1)*10);

		ros::spinOnce();

		loop_rate.sleep();

		time = time + 0.001;

	}

	return 0;

}