#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include "Settings.hh"

using namespace std;

using boost::property_tree::ptree;

class is_int : public boost::static_visitor<bool>
{
public:
    bool operator()(double & i) const{ return false;}
    bool operator()(int & i) const{ return true;}
    bool operator()(const std::string & str) const{return false;}
};
class is_double : public boost::static_visitor<bool>
{
public:
    bool operator()(double & i) const{ return true;}
    bool operator()(int & i) const{ return false;}
    bool operator()(const std::string & str) const{return false;}
};

class is_string : public boost::static_visitor<bool>
{
public:
    bool operator()(double & i) const{ return false;}
    bool operator()(int & i) const{ return false;}
    bool operator()(const std::string & str) const{return true;}
};
//constructor

double_int_string Settings::load_setting_from_xml(std::string setting_name, double_int_string setting_value){
    double_int_string OUT_ = setting_value;
    if (boost::apply_visitor( is_double(), setting_value )){
        OUT_ = pt.get("settings."+setting_name, boost::get<double>(setting_value));
    }
    else if(boost::apply_visitor( is_int(), setting_value )){
        OUT_ =pt.get("settings."+setting_name, boost::get<int>(setting_value));
    }
    else if(boost::apply_visitor( is_string(), setting_value )){
        OUT_ =pt.get("settings."+setting_name, boost::get<string>(setting_value));
    }
    return OUT_;
}

//settings
std::string Settings::loadedFrom = "defaultValue";
std::string Settings::filename = "settings.xml";
std::string Settings::prefix = "";
std::string Settings::path = "";
Settings::Settings(){
    load_default_settings();
	init();

}
Settings::Settings(std::string str){
    this->filename = str;
    load_default_settings();
	init();
}
void Settings::load_default_settings(){
    /**
     * TIME STEP Management
     * time_step : control the global time_step in ms
     * time_step_eventManager : control the event manager's time_step (in multiple of time_step)
     */
    this->sets["time_step"] = 1; //ms
    this->sets["number_of_repeat"] = 1;
    
    this->sets["angle_transfer_function"] = 0;

    /**
     * Parameter controlling the muscle parameters.
     * If set to 1 then parameters should be defined for left and right leg (example hf_fmax_left or hf_fmax_right)
     * If set to 0 then parameters should be defined once for both legs (example hf_fmax)
     */
    this->sets["assymetric_gait"] = 0;
	    /** 
     * The transfer function acts on any entity (i.e. interneuron sensory interneuron) 
     * The transfer_function_type is the type of transfer_function to be used 
     * 1 : threshold transfer function
     * 2 : sigmoid transfer function (not yet implemented)
     */
    this->sets["transfer_function"] = 0;
    this->sets["transfer_function_type"] = 1;
    
    this->sets["running_mode"] = "normal", // execution mode
    this->sets["muscle_step_number"] = 1;
    this->sets["torque_soft_limit"] = 1;
}

void Settings::printAll(){
    for (auto &kv: sets){
        cout << kv.first << "=" << kv.second << endl;
    }
}

void Settings::init(){

        this->loadedFrom = "externalFile";
        if ( path == "")
            path = "../../conf"+this->prefix+"/settings/";
        cout << endl << "\tloading settings from external settings file : " << path << filename << endl;
        read_xml(path+filename, pt);
    
    for(auto &kv : this->sets)
    {

            kv.second = load_setting_from_xml(kv.first, kv.second);
    }
}
