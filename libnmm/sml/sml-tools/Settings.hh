#pragma once
#ifndef __SETTINGS_HH__
#define __SETTINGS_HH__

#include <string>
#include <map>
#include <boost/property_tree/ptree.hpp>
#include <boost/variant.hpp>
#include <boost/algorithm/string.hpp>

typedef boost::variant< double, int, std::string > double_int_string;
class Settings
{
private:
    boost::property_tree::ptree pt;
public:
	static std::map<std::string, double_int_string> sets;
	static std::string loadedFrom;
	static std::string config;
    static std::string filename;
    static std::string path;
	static std::string prefix;
    void load_default_settings();
    template<class T> static T get(std::string str);
	template<class T> static void set(std::string str, T);
	void static printAll();
	Settings();
	Settings(std::string str);
    double_int_string load_setting_from_xml(std::string setting_name,  double_int_string setting_value);
	void init();
};

template<class T> T Settings::get(std::string str)
{
    return boost::get<T>(sets[str]);
}
template<class T> void Settings::set(std::string str, T value)
{
	sets[str] = value;
}

#endif /* __SETTINGS_HH__ */
