#include "Sensor.hh"
#include "Muscle.hh"
#include <boost/any.hpp>
#include <cstdlib>

#include <sml/sml-tools/RungeKutta.hh>

using namespace std;

double Sensor::tf(double x){
    static double y;
    static int noise_sensor = Settings::get<int>("noise_sensor");
    y=x;
    
    return y;
}
/*


////////////////// MUSCLE SENSORS //////////////////


*/

// Muscle Lenght Sensor
double MuscleLengthSensor::update(){
    //change was: return gain*(muscle->l_CE/muscle->l_opt - offset);
    static double out;
    out = gain*(muscle->getL_CE()/muscle->l_opt - offset);
    if(out <0)
        out = 0.0;
    return tf(out);
}
// Muscle Force Sensor
double MuscleForceSensor::update(){
    return tf(gain * muscle->getForce() / muscle->F_max);
}
// Muscle Type Ia Fiber Sensor
double MuscleIaSensor::update(){

    // Ia = kv*(v_norm)^pv + kdI*dnorm + knI*f(V) + constI;
    
    double muscleLength =  ((muscle->getL_CE() - muscle->l_opt)/muscle->l_opt);

    muscleLength = (muscleLength > 0 ) ?  muscleLength : 0.0;

    double muscleVelocity = pow(std::abs(muscle->getV_CE()),this->pv)*muscle->getV_CE()/muscle->getV_CE();

    return (this->kv*muscleVelocity)  +  (this->kdI*(muscleLength)) + (this->knI*muscle->getA() ) + this->constI;
}

// Muscle Type Ia Fiber Sensor
double MuscleIbSensor::update(){

    // Ia = kv*(v_norm)^pv + kdI*dnorm + knI*f(V) + constI;

    return this->kf*muscle->getForce()/muscle->getFmax();;
}

// Muscle Type Ia Fiber Sensor
double MuscleIISensor::update(){

    // Ia = kv*(v_norm)^pv + kdI*dnorm + knI*f(V) + constI;

    double muscleLength =  ((muscle->getL_CE() - muscle->l_opt)/muscle->l_opt);

    muscleLength = (muscleLength > 0 ) ?  muscleLength : 0.0;

    return (this->kdII*(muscleLength)) + (this->knII*muscle->getA() ) + this->constII;
}

