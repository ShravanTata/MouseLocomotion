#include "SmlMouse.hh"
#include "Muscle.hh"
#include "MuscleJoint.hh"
#include "Joint.hh"
#include "Sensor.hh"
#include <sml/sml-tools/Settings.hh>
#include <boost/xpressive/xpressive.hpp>
#include <string>

using namespace boost::xpressive;
using namespace MOUSE;

typedef std::map<std::string, std::map<std::string, Muscle>>::iterator it_muscle;


SmlMouse::SmlMouse():
Constant(CONSTANT::NUMBER, 0.0),
Input(INPUT::NUMBER, 0.0),
Output(OUTPUT::NUMBER, 0.0),
time_step(Settings::get<int>("time_step")) {
}

extern bool debug;
void SmlMouse::init() {
    debug = false;
    cout << endl;
    cout << "=========================" << endl;
    cout << "=========================" << endl;
    cout << "Code of branch ver/MOUSE MODELLING" << endl;
    cout << "=========================" << endl;
    cout << "=========================" << endl;

    ConstantInit();
    print_debug("[ok] : ConstantInit Init (SmlMouse.cc)");

    InputInit();
    print_debug("[ok] : Input Init (SmlMouse.cc)");

    // Constant Initilisation
    initialiseConstant();

    // 1. Initialise Joints
    initialiseJoints();
    print_debug("[ok] : joints initialisation (SmlMouse.cc)");

    // 2. Initialise Muscles
    initialiseMuscles();
    print_debug("[ok] : muscles initialisation (SmlMouse.cc)");

    // 3. Initialise Sensors
    initialiseSensors();
    print_debug("[ok] : sensors initialisation (SmlMouse.cc)");

    print_debug("[ok] : Initilisation complete (SmlMouse.cc)");

}


int SmlMouse::step() {

    InputUpdate();

    // Muscles to mechanical system
    step_JOINT(dt);

    step_MTU_to_TORQUE(dt);

    step_TORQUE_to_JOINT();

    // Sensors to Neural control
    step_SENSORS();

    /// Neural control to Muscles
    // step_SPINE_TO_MTU(dt);

    return 0;
}


// Initialise constant
void SmlMouse::initialiseConstant()
{
    dt = double(time_step) / 1000.0;
}

// 1. Initialise Joints
void SmlMouse::initialiseJoints()
{

    for (

        unsigned joint = JOINT::FIRST_JOINT,
        input = INPUT::FIRST_JOINT,
        output = OUTPUT::FIRST_JOINT,
        joint_pos_ref = CONSTANT::FIRST_REF_POS_JOINT,
        joint_pos_min = CONSTANT::FIRST_MIN_POS_JOINT,
        joint_pos_max = CONSTANT::FIRST_MAX_POS_JOINT;

        output <= OUTPUT::FIRST_JOINT + 5;

        joint++,
        input++,
        output++,
        joint_pos_ref++,
        joint_pos_min++,
        joint_pos_max++)

    {

        JointMomentArms::MOMENT_ARM joint_type;

        if ((input == INPUT::ANGLE_HIP_RIGHT) || (input == INPUT::ANGLE_HIP_LEFT))
        {   //Change
            //joint_type = JointMomentArms::CONSTANT;
            joint_type = JointMomentArms::GEYER;
            std::cout << "Check Geyer : CHANGED" << std::endl;
        }
        else
        {
            joint_type = JointMomentArms::GEYER;
            std::cout << "Check Geyer" << std::endl;
        }

        std::cout << JOINT::toString((JOINT::Joints)joint) << " : Min : " << joint_pos_min << " Max : " << joint_pos_max << std::endl;

        joints[joint] = new Joint(
            joint_type,
            &Input[input],
            &Output[output],
            Constant[joint_pos_ref],
            Constant[joint_pos_min],
            Constant[joint_pos_max]);

        joints[joint]->name = JOINT::toString((JOINT::Joints)joint);
    }
}

// 2. Initialise Muscles
void SmlMouse::initialiseMuscles()
{
    double clockwise = 1.0;
            double cclockwise = -1.0;

    // PMA
            double r_PMA        = cclockwise * 0.0019686;

            double phi_ref_PMA = Constant[CONSTANT::REF_PHI_PMA];

            double phi_max_PMA = Constant[CONSTANT::MAX_PHI_PMA];

            std::vector<double> co_efficient_pma {0.007937  ,  -0.29882 ,  -0.076719   ,  0.57424   ,  0.73717};

    // CF

            double r_CF     = clockwise * 0.00252498;

            double phi_ref_CF = Constant[CONSTANT::REF_PHI_CF];

            double phi_max_CF = Constant[CONSTANT::MAX_PHI_CF];

            std::vector<double> co_efficient_cf {0.0048891 ,    0.20821,    -0.31945  ,  -0.23736   ,  0.96201};

    // SM - BiArticular

            double r_SM_hip = clockwise * 0.00558936;

            double r_SM_knee = clockwise * 0.00574158;

            double phi_ref_SM_hip = Constant[CONSTANT::REF_PHI_SM_HIP];

            double phi_max_SM_hip = Constant[CONSTANT::MAX_PHI_SM_HIP];

            double phi_ref_SM_knee = Constant[CONSTANT::REF_PHI_SM_KNEE];

            double phi_max_SM_knee = Constant[CONSTANT::MAX_PHI_SM_KNEE];

            std::vector<double> co_efficient_sm_hip {0.042135  ,   0.09435 ,   -0.57917    ,  0.2002    , 0.98294};

            std::vector<double> co_efficient_sm_knee {-0.031928 ,  -0.023103  ,  -0.13914  ,  -0.86126  ,  0.093956};

            // POP

            double r_POP = clockwise * 0.00087184;

            double phi_ref_POP = Constant[CONSTANT::REF_PHI_POP];

            double phi_max_POP = Constant[CONSTANT::MAX_PHI_POP];

            std::vector<double> co_efficient_pop {-0.32718  ,   -1.2038  ,  -0.67271 , -0.0014029 ,   -0.88994};

    // RF

            double r_RF = cclockwise * 0.00162329;

            double phi_ref_RF = Constant[CONSTANT::REF_PHI_RF];

            double phi_max_RF = Constant[CONSTANT::MAX_PHI_RF];

            std::vector<double> co_efficient_rf {-0.10206 ,   -0.59432   ,  -1.1763  ,  -0.72712  ,   0.85665};

    // TA

            double r_TA = cclockwise * 0.0007748;

            double phi_ref_TA = Constant[CONSTANT::REF_PHI_TA];

            double phi_max_TA = Constant[CONSTANT::MAX_PHI_TA];

            std::vector<double> co_efficient_ta {-0.13734   ,  -0.2497 ,   -0.35413 ,    0.17162   ,  0.98346};

    // SOL

            double r_SOL = clockwise * 0.001840;

            double phi_ref_SOL = Constant[CONSTANT::REF_PHI_SOL];

            double phi_max_SOL = Constant[CONSTANT::MAX_PHI_SOL];

            std::vector<double> co_efficient_sol { 0.033427  ,  0.064567  ,  -0.51264 ,   0.047817     ,0.99905};

    // LG

            double r_LG_knee = clockwise * 0.001221;

            double r_LG_ankle = clockwise * 0.001996;

            double phi_ref_LG_knee = Constant[CONSTANT::REF_PHI_LG_KNEE];

            double phi_ref_LG_ankle = Constant[CONSTANT::REF_PHI_LG_ANKLE];

            double phi_max_LG_knee = Constant[CONSTANT::MAX_PHI_LG_KNEE];

            double phi_max_LG_ankle = Constant[CONSTANT::MAX_PHI_LG_ANKLE];

            std::vector<double> co_efficient_lg_knee {0.19982  ,    0.9702  ,   0.89243 ,  -0.068951  ,   0.80363};

            std::vector<double> co_efficient_lg_ankle {0.03437  ,   0.05088  ,  -0.50017 , -0.0009494  ,   1};

    // Create a pointer for muscles
    Muscle  * m;

    string name;

    //CHANGE
    static double muscle_parameters_default[7][8] = {
        //pma           cf          sm          pop         rf          ta          sol         lg
        { 1.338,        0.554,      1.916,      0.307,      4.162,      2.422,      0.591,      3.784   }, // fmax
        { 0.00697,      0.01137,    0.01165,    0.00206,    0.00534,    0.0049,     0.00316,    0.00541 }, // l_opts
        { 0.00501,      0.00307,    0.00409,    0.00203,    0.00853,    0.0118,     0.0074,     0.01389}, // l_slacks
        { 12,           12,         12,         12,         12,         12,         12,         12 }, // v_maxes
        { 0.00003285,   0.00002216, 0.00007861, 0.00000222, 0.00000782, 0.000004217, 0.000000658, 0.0000072 }, // masses
        { 0.7288,       1,          1,          1,          0.7226,     1,     1,     0.6984 }, // pennation
        { 0.5,          0.5,        0.5,        0.5,        0.5,        0.5,        0.5,        0.5 }, // typeIfiber
    };


        double* fmax[8];
        double* l_opts[8];
        double* l_slacks[8];
        double* v_maxes[8];
        double* masses[8];

        double* pennation[8];
        double* typeIfiber[8];

        for (int i = 0; i < 8; i++) {

            fmax[i] = &muscle_parameters_default[0][i];
            l_opts[i] = &muscle_parameters_default[1][i];
            l_slacks[i] = &muscle_parameters_default[2][i];
            v_maxes[i] = &muscle_parameters_default[3][i];
            masses[i] = &muscle_parameters_default[4][i];
            pennation[i] = &muscle_parameters_default[5][i];
            typeIfiber[i] = &muscle_parameters_default[6][i];

        }


    name = "pma";
    m = new Muscle(SIDE::LEFT,name,*(l_slacks[0]),*(l_opts[0]),*(v_maxes[0]),*(fmax[0]), *(pennation[0]),*(typeIfiber[0])); //pma
    m->musclejoints.push_back( new MuscleJoint(joints[JOINT::HIP_LEFT], m ,r_PMA,phi_ref_PMA,phi_max_PMA,co_efficient_pma));
    muscles[MUSCLES_MOUSE::LEFT_PMA] = m;

    name = "cf";    
    m = new Muscle(SIDE::LEFT,name,*(l_slacks[1]),*(l_opts[1]),*(v_maxes[1]),*(fmax[1]), *(pennation[1]),*(typeIfiber[1])); //cf
    m->musclejoints.push_back( new MuscleJoint(joints[JOINT::HIP_LEFT], m ,r_CF,phi_ref_CF,phi_max_CF,co_efficient_cf));
    muscles[MUSCLES_MOUSE::LEFT_CF] = m;

    name = "sm";
    m = new Muscle(SIDE::LEFT, name, *(l_slacks[2]), *(l_opts[2]), *(v_maxes[2]), *(fmax[2]), *(pennation[2]), *(typeIfiber[2])); // sm
    m->musclejoints.push_back(new MuscleJoint(joints[JOINT::HIP_LEFT], m , r_SM_hip, phi_ref_SM_hip, phi_max_SM_hip,co_efficient_sm_hip));
    m->musclejoints.push_back( new MuscleJoint(joints[JOINT::KNEE_LEFT], m , r_SM_knee, phi_ref_SM_knee, phi_max_SM_knee,co_efficient_sm_knee));
    muscles[MUSCLES_MOUSE::LEFT_SM] = m;

    name = "pop";
    m = new Muscle(SIDE::LEFT, name, *(l_slacks[3]), *(l_opts[3]), *(v_maxes[3]), *(fmax[3]), *(pennation[3]), *(typeIfiber[3])); // pop
    m->musclejoints.push_back(new MuscleJoint(joints[JOINT::KNEE_LEFT], m , r_POP, phi_ref_POP, phi_max_POP,co_efficient_pop));
    muscles[MUSCLES_MOUSE::LEFT_POP] = m;

    name = "rf";
    m = new Muscle(SIDE::LEFT, name, *(l_slacks[4]), *(l_opts[4]), *(v_maxes[4]), *(fmax[4]), *(pennation[4]), *(typeIfiber[4])); // rf
    m->musclejoints.push_back(new MuscleJoint(joints[JOINT::KNEE_LEFT], m , r_RF, phi_ref_RF, phi_max_RF,co_efficient_rf));
    muscles[MUSCLES_MOUSE::LEFT_RF] = m;

    name = "ta";
    m = new Muscle(SIDE::LEFT, name, *(l_slacks[5]), *(l_opts[5]), *(v_maxes[5]), *(fmax[5]), *(pennation[5]), *(typeIfiber[5])); // ta
    m->musclejoints.push_back(new MuscleJoint(joints[JOINT::ANKLE_LEFT], m , r_TA, phi_ref_TA, phi_max_TA,co_efficient_ta));
    muscles[MUSCLES_MOUSE::LEFT_TA] = m;

    name = "sol";
    m = new Muscle(SIDE::LEFT, name, *(l_slacks[6]), *(l_opts[6]), *(v_maxes[6]), *(fmax[6]), *(pennation[6]), *(typeIfiber[6])); // sol
    m->musclejoints.push_back(new MuscleJoint(joints[JOINT::ANKLE_LEFT], m , r_SOL, phi_ref_SOL, phi_max_SOL,co_efficient_sol));
    muscles[MUSCLES_MOUSE::LEFT_SOL] = m;

    name = "lg";
    m = new Muscle(SIDE::LEFT, name, *(l_slacks[7]), *(l_opts[7]), *(v_maxes[7]), *(fmax[7]), *(pennation[7]), *(typeIfiber[7])); // lg
    m->musclejoints.push_back(new MuscleJoint(joints[JOINT::KNEE_LEFT], m , r_LG_knee, phi_ref_LG_knee, phi_max_LG_knee,co_efficient_lg_knee));
    m->musclejoints.push_back( new MuscleJoint(joints[JOINT::ANKLE_LEFT], m , r_LG_ankle, phi_ref_LG_ankle, phi_max_LG_ankle,co_efficient_lg_ankle));
    muscles[MUSCLES_MOUSE::LEFT_LG] = m;


    name = "pma";
    m = new Muscle(SIDE::RIGHT,name,*(l_slacks[0]),*(l_opts[0]),*(v_maxes[0]),*(fmax[0]), *(pennation[0]),*(typeIfiber[0])); //pma
    m->musclejoints.push_back( new MuscleJoint(joints[JOINT::HIP_RIGHT], m ,r_PMA,phi_ref_PMA,phi_max_PMA,co_efficient_pma));
    muscles[MUSCLES_MOUSE::RIGHT_PMA] = m;

    name = "cf";    
    m = new Muscle(SIDE::RIGHT,name,*(l_slacks[1]),*(l_opts[1]),*(v_maxes[1]),*(fmax[1]), *(pennation[1]),*(typeIfiber[1])); //cf
    m->musclejoints.push_back( new MuscleJoint(joints[JOINT::HIP_RIGHT], m ,r_CF,phi_ref_CF,phi_max_CF,co_efficient_cf));
    muscles[MUSCLES_MOUSE::RIGHT_CF] = m;

    name = "sm";
    m = new Muscle(SIDE::RIGHT, name, *(l_slacks[2]), *(l_opts[2]), *(v_maxes[2]), *(fmax[2]), *(pennation[2]), *(typeIfiber[2])); // sm
    m->musclejoints.push_back(new MuscleJoint(joints[JOINT::HIP_RIGHT], m , r_SM_hip, phi_ref_SM_hip, phi_max_SM_hip,co_efficient_sm_hip));
    m->musclejoints.push_back( new MuscleJoint(joints[JOINT::KNEE_RIGHT], m , r_SM_knee, phi_ref_SM_knee, phi_max_SM_knee,co_efficient_sm_knee));
    muscles[MUSCLES_MOUSE::RIGHT_SM] = m;

    name = "pop";
    m = new Muscle(SIDE::RIGHT, name, *(l_slacks[3]), *(l_opts[3]), *(v_maxes[3]), *(fmax[3]), *(pennation[3]), *(typeIfiber[3])); // pop
    m->musclejoints.push_back(new MuscleJoint(joints[JOINT::KNEE_RIGHT], m , r_POP, phi_ref_POP, phi_max_POP,co_efficient_pop));
    muscles[MUSCLES_MOUSE::RIGHT_POP] = m;

    name = "rf";
    m = new Muscle(SIDE::RIGHT, name, *(l_slacks[4]), *(l_opts[4]), *(v_maxes[4]), *(fmax[4]), *(pennation[4]), *(typeIfiber[4])); // rf
    m->musclejoints.push_back(new MuscleJoint(joints[JOINT::KNEE_RIGHT], m , r_RF, phi_ref_RF, phi_max_RF,co_efficient_rf));
    muscles[MUSCLES_MOUSE::RIGHT_RF] = m;

    name = "ta";
    m = new Muscle(SIDE::RIGHT, name, *(l_slacks[5]), *(l_opts[5]), *(v_maxes[5]), *(fmax[5]), *(pennation[5]), *(typeIfiber[5])); // ta
    m->musclejoints.push_back(new MuscleJoint(joints[JOINT::ANKLE_RIGHT], m , r_TA, phi_ref_TA, phi_max_TA,co_efficient_ta));
    muscles[MUSCLES_MOUSE::RIGHT_TA] = m;

    name = "sol";
    m = new Muscle(SIDE::RIGHT, name, *(l_slacks[6]), *(l_opts[6]), *(v_maxes[6]), *(fmax[6]), *(pennation[6]), *(typeIfiber[6])); // sol
    m->musclejoints.push_back(new MuscleJoint(joints[JOINT::ANKLE_RIGHT], m , r_SOL, phi_ref_SOL, phi_max_SOL,co_efficient_sol));
    muscles[MUSCLES_MOUSE::RIGHT_SOL] = m;

    name = "lg";
    m = new Muscle(SIDE::RIGHT, name, *(l_slacks[7]), *(l_opts[7]), *(v_maxes[7]), *(fmax[7]), *(pennation[7]), *(typeIfiber[7])); // lg
    m->musclejoints.push_back(new MuscleJoint(joints[JOINT::KNEE_RIGHT], m , r_LG_knee, phi_ref_LG_knee, phi_max_LG_knee,co_efficient_lg_knee));
    m->musclejoints.push_back( new MuscleJoint(joints[JOINT::ANKLE_RIGHT], m , r_LG_ankle, phi_ref_LG_ankle, phi_max_LG_ankle,co_efficient_lg_ankle));
    muscles[MUSCLES_MOUSE::RIGHT_LG] = m;

}

// 3. Initialise Sensors
void SmlMouse::initialiseSensors()
{

    // Muscle Sensors
    MuscleIaSensor * mIas;
    MuscleIbSensor * mIbs;
    MuscleIISensor * mIIs;

    for (auto& muscle : muscles) {
        mIas = new MuscleIaSensor(muscle);
        mIbs = new MuscleIbSensor(muscle);
        mIIs = new MuscleIISensor(muscle);
        sensors[muscle->getName() + "_Ia"] = mIas;
        sensors[muscle->getName() + "_Ib"] = mIbs;
        sensors[muscle->getName() + "_II"] = mIIs;
    }
}


double SmlMouse::getTime() {
    return time;
}

//compute l_MTC
void SmlMouse::step_MTU_to_TORQUE(double dt) {
    static int muscle_step_number = Settings::get<int>("muscle_step_number");
    static int i;
    for (auto&  muscle : muscles) {
        muscle->ApplyForce();
        for (i = 0; i < muscle_step_number; i++) {
            muscle->step(dt / muscle_step_number);
        }
    }
    print_debug("[ok] : compute muscle (Sml.cc)");
}

//compute all the joint angles
void SmlMouse::step_JOINT(double dt) {
    for (auto&  joint : joints) {
        joint->step(dt);
    }
    print_debug("[ok] : compute angle (Sml.cc)");
}

void SmlMouse::step_TORQUE_to_JOINT() {
    static int torque_soft_limit = Settings::get<int>("torque_soft_limit");
    for (auto&  joint : joints) {
        if (torque_soft_limit) joint->ComputeTorqueSoftLimit();
        joint->ComputeTorque();
    }
    print_debug("[ok] : compute torque (Sml.cc)");
}

void SmlMouse::step_SENSORS() {
    for (auto &sen : sensors) {
        sen.second->step();
    }

    print_debug("[ok] : compute sensors (Sml.cc)");
}

void SmlMouse::step_SPINE_TO_MTU(double dt) {

}

SmlMouse::~SmlMouse() {

}