#pragma once
#ifndef __SENSOR_HH__
#define __SENSOR_HH__

/*!
 * \file Sensor.hh
 * \brief Sensor description (touch sensor / muscle length / muscle force / foot / ...)
 * \author efx
 * \version 0.1
 */

#include <vector>
#include <string>
#include <cmath>
#include <map>
#include <sml/sml-tools/RungeKutta.hh>

class Muscle;
class Joint;

class Sensor {
public:
	double value;
    void step(){
    	value = update();
    };
    const double& get(){
    	return value;
    }
    double tf(double);
private:
	virtual double update() = 0;
};


/**
 * 
 * MuscleLengthSensor class
 * 
 * 
 */
class MuscleLengthSensor: public Sensor{
public:
	Muscle * muscle;
	double& offset;
	double& gain;
	MuscleLengthSensor(Muscle * muscle, double &offset, double &gain):muscle(muscle),offset(offset),gain(gain){}
private:
	double update();
};

/**
 * 
 * MuscleForceSensor class
 * 
 * 
 */
class MuscleForceSensor: public Sensor{
	Muscle * muscle;
	double& gain;
public:
	MuscleForceSensor(Muscle * muscle, double &gain):muscle(muscle), gain(gain){}
private:
	double update();
};


/**
 * 
 * Muscle Ia fiber Sensor class
 * 
 * 
 */

class MuscleIaSensor : public Sensor{
public:
	// Constructor
	MuscleIaSensor(Muscle * muscle):muscle(muscle){}
	// Variables
	Muscle * muscle;
private:
	double update();
	// Variables
	const double pv = 0.6;
	const double kv = 6.2;
	const double kdI = 2;
	const double knI = 0.06;
	const double constI = 0.001; // To be checked : str
};

/**
 * 
 * Muscle Ib fiber Sensor class
 * 
 * 
 */

class MuscleIbSensor : public Sensor{
public:
	// Constructor
	MuscleIbSensor(Muscle * muscle):muscle(muscle){}
	// Variables
	Muscle * muscle;
private:
	double update();
	// Variables
	const double kf = 1;
};

/**
 * 
 * Muscle II fiber Sensor class
 * 
 * 
 */

class MuscleIISensor : public Sensor{
public:
	// Constructor
	MuscleIISensor(Muscle * muscle):muscle(muscle){}
	// Variables
	Muscle * muscle;
private:
	double update();
	// Variables
	const double kdII = 1.5;
	const double knII = 0.06;
	const double constII = 0.001; // To be checked : str
};



#endif /* __SENSOR_HH__ */

