#include <sml/types/musculoSkeletalSystem.hh>
#include <sml/types/types.h>
#include <string>
using namespace std;


namespace MUSCLES_MOUSE{
    bool IS_LEFT(int i){
        if (i <= LEFT_LG && i >= LEFT_PMA)
            return true;
        else
            return false;
    }
    bool IS_RIGHT(int i){
        if (i <= RIGHT_LG && i >= RIGHT_PMA)
            return true;
        else
            return false;
    }
    std::string toString(int id){
        string result;
        switch(id){
            case LEFT_PMA:
                result="LEFT_PMA";
                break;
            case LEFT_CF:
                result="LEFT_CF";
                break;
            case LEFT_SM:
                result="LEFT_SM";
                break;
            case LEFT_POP:
                result="LEFT_POP";
                break;
            case LEFT_RF:
                result="LEFT_RF";
                break;
            case LEFT_TA:
                result="LEFT_TA";
                break;
            case LEFT_SOL:
                result="LEFT_SOL";
                break;
            case LEFT_LG:
                result="LEFT_LG";
                break;
            case RIGHT_PMA:
                result="RIGHT_PMA";
                break;
            case RIGHT_CF:
                result="RIGHT_CF";
                break;
            case RIGHT_SM:
                result="RIGHT_SM";
                break;
            case RIGHT_POP:
                result="RIGHT_POP";
                break;
            case RIGHT_RF:
                result="RIGHT_RF";
                break;
            case RIGHT_TA:
                result="RIGHT_TA";
                break;
            case RIGHT_SOL:
                result="RIGHT_SOL";
                break;
            case RIGHT_LG:
                result="RIGHT_LG";
                break;
            default:
                result="not found";
        }
        return result;
    }
    int toID(string str){
        int result = -1;
        if(str=="LEFT_PMA" || str=="left_pma")
            result = LEFT_PMA;
        if(str=="LEFT_CF" || str=="left_cf")
            result = LEFT_CF;
        if(str=="LEFT_SM" || str=="left_sm")
            result = LEFT_SM;
        if(str=="LEFT_POP" || str=="left_pop")
            result = LEFT_POP;
        if(str=="LEFT_RF" || str=="left_rf")
            result = LEFT_RF;
        if(str=="LEFT_TA" || str=="left_ta")
            result = LEFT_TA;
        if(str=="LEFT_SOL" || str=="left_sol")
            result = LEFT_SOL;
        if(str=="LEFT_LG" || str=="left_lg")
            result = LEFT_LG;

        if(str=="RIGHT_PMA" || str=="right_pma")
            result = RIGHT_PMA;
        if(str=="RIGHT_CF" || str=="right_cf")
            result = RIGHT_CF;
        if(str=="RIGHT_SM" || str=="right_sm")
            result = RIGHT_SM;
        if(str=="RIGHT_POP" || str=="right_pop")
            result = RIGHT_POP;
        if(str=="RIGHT_RF" || str=="right_rf")
            result = RIGHT_RF;
        if(str=="RIGHT_TA" || str=="right_ta")
            result = RIGHT_TA;
        if(str=="RIGHT_SOL" || str=="right_sol")
            result = RIGHT_SOL;
        if(str=="RIGHT_LG" || str=="right_lg")
            result = RIGHT_LG;
        return result;
    }
}