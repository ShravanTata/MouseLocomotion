#ifndef __AIRI_TYPES_MODEL_CONSTANTS_H__
#define __AIRI_TYPES_MODEL_CONSTANTS_H__

namespace MOUSE{
namespace CONSTANT{
    typedef enum
    {
        /*****************
           Segment constants
         *****************/
        LENGTH_THIGH,
        LENGTH_SHIN,
        LENGTH_FOOT,
        WEIGHT_TRUNK,
        WEIGHT_THIGH,
        WEIGHT_SHIN,
        WEIGHT_FOOT,
        MODEL_WEIGHT,
        MODEL_HEIGHT,
        /*****************
           Joint constants 
         *****************/
        REF_POS_HIP_LEFT,
        REF_POS_KNEE_LEFT,
        REF_POS_ANKLE_LEFT,
        REF_POS_HIP_RIGHT,
        REF_POS_KNEE_RIGHT,
        REF_POS_ANKLE_RIGHT,
        MAX_POS_HIP_LEFT,
        MAX_POS_KNEE_LEFT,
        MAX_POS_ANKLE_LEFT,
        MAX_POS_HIP_RIGHT,
        MAX_POS_KNEE_RIGHT,
        MAX_POS_ANKLE_RIGHT,
        MIN_POS_HIP_LEFT,
        MIN_POS_KNEE_LEFT,
        MIN_POS_ANKLE_LEFT,
        MIN_POS_HIP_RIGHT,
        MIN_POS_KNEE_RIGHT,
        MIN_POS_ANKLE_RIGHT,
        REF_POS_HIPCOR_LEFT,
        REF_POS_HIPCOR_RIGHT,
        MAX_POS_HIPCOR_LEFT,
        MAX_POS_HIPCOR_RIGHT,
        MIN_POS_HIPCOR_LEFT,
        MIN_POS_HIPCOR_RIGHT,
        /*****************
           Muscle constants 
         *****************/
        // Mono-articular muscle, variables order R,PHI_MAX,PHI_REF,L_OPT,L_SLACK,V_MAX,MASS,PENNATION PERCENTAGE, TYPE1FIBER PERCENTAGE, DIRECTION

        R_PMA,
        R_CF,
        R_POP,
        R_RF,
        R_TA,
        R_SOL,
        MAX_PHI_PMA,
        MAX_PHI_CF,
        MAX_PHI_POP,
        MAX_PHI_RF,
        MAX_PHI_TA,
        MAX_PHI_SOL,
        REF_PHI_PMA,
        REF_PHI_CF,
        REF_PHI_POP,
        REF_PHI_RF,
        REF_PHI_TA,
        REF_PHI_SOL,    
        OPT_L_PMA,
        OPT_L_CF,
        OPT_L_POP,
        OPT_L_RF,
        OPT_L__TA,
        OPT_L__SOL,
        SLACK_L_PMA,
        SLACK_L_CF,
        SLACK_L_POP,
        SLACK_L_RF,
        SLACK_L_TA,
        SLACK_L_SOL,
        MAX_V_PMA,
        MAX_V_CF,
        MAX_V_POP,
        MAX_V_RF,
        MAX_V__TA,
        MAX_V__SOL,
        MASS_PMA,
        MASS_CF,
        MASS_POP,
        MASS_RF,
        MASS_TA,
        MASS_SOL,
        PENNATION_PMA,
        PENNATION_CF,
        PENNATION_POP,
        PENNATION_RF,
        PENNATION_TA,
        PENNATION_SOL,
        TYPE1FIBER_PMA,
        TYPE1FIBER_CF,
        TYPE1FIBER_POP,
        TYPE1FIBER_RF,
        TYPE1FIBER_TA,
        TYPE1FIBER_SOL,
        DIRECTION_PMA,
        DIRECTION_CF,
        DIRECTION_POP,
        DIRECTION_RF,
        DIRECTION_TA,
        DIRECTION_SOL,        
        // bi-articular muscle, variables order R,PHI_MAX,PHI_REF,L_OPT,L_SLACK,V_MAX,MASS,PENNATION PERCENTAGE, TYPE1FIBER PERCENTAGE, DIRECTION
        // R,PHI_MAX, PHI_REF have two variable one for each joint on which it acts (order : distal-proximal)        


        R_SM_KNEE,
        R_SM_HIP,
        R_LG_ANKLE,
        R_LG_KNEE,
        MAX_PHI_SM_KNEE,
        MAX_PHI_SM_HIP,
        MAX_PHI_LG_ANKLE,
        MAX_PHI_LG_KNEE,
        REF_PHI_SM_KNEE,
        REF_PHI_SM_HIP,
        REF_PHI_LG_ANKLE,
        REF_PHI_LG_KNEE,        
        OPT_L_SM,
        OPT_L_LG,
        SLACK_L_SM,
        SLACK_L_LG,
        MAX_V_SM,
        MAX_V_LG,
        MASS_SM,
        MASS_LG,
        PENNATION_SM,
        PENNATION_LG,
        DIRECTION_SM,
        DIRECTION_LG,
        FIRST = LENGTH_THIGH,
        LAST = DIRECTION_LG,
    } ConstantsMouse;

    const int NUMBER = CONSTANT::LAST - CONSTANT::FIRST + 1;
    
    const unsigned FIRST_REF_POS_JOINT = REF_POS_HIP_LEFT;
    const unsigned LAST_REF_POS_JOINT = REF_POS_ANKLE_RIGHT;
    const unsigned FIRST_MIN_POS_JOINT = MIN_POS_HIP_LEFT;
    const unsigned LAST_MIN_POS_JOINT = MIN_POS_ANKLE_RIGHT;
    const unsigned FIRST_MAX_POS_JOINT = MAX_POS_HIP_LEFT;
    const unsigned LAST_MAX_POS_JOINT = MAX_POS_ANKLE_RIGHT;

}
}

#endif