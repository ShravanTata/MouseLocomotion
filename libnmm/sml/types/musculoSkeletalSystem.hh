#ifndef __AIRI_TYPES_MUSCLES_H__
#define __AIRI_TYPES_MUSCLES_H__

namespace MUSCLES_MOUSE{
    typedef enum
    {
        LEFT_PMA,
        LEFT_CF,
        LEFT_SM,
        LEFT_POP,
        LEFT_RF,
        LEFT_TA,
        LEFT_SOL,
        LEFT_LG,
        RIGHT_PMA,
        RIGHT_CF,
        RIGHT_SM,
        RIGHT_POP,
        RIGHT_RF,
        RIGHT_TA,
        RIGHT_SOL,
        RIGHT_LG,
        LAST = RIGHT_LG,
        FIRST = LEFT_PMA,
    } MusclesMouse;
    const int NUMBER = LAST - FIRST + 1;
    int toID(std::string str);
    bool IS_RIGHT(int i);
    bool IS_LEFT(int i);
    std::string toString(int id);
    /*
    std::map<MUSCLES::Muscles,std::string> Names ={
        { MUSCLES::LEFT_HF, "LEFT_HF" },
        { MUSCLES::LEFT_GLU, "LEFT_GLU" },
        { MUSCLES::LEFT_VAS, "LEFT_VAS" },
        { MUSCLES::LEFT_HAM, "LEFT_HAM" },
        { MUSCLES::LEFT_GAS, "LEFT_GAS" },
        { MUSCLES::LEFT_SOL, "LEFT_SOL" },
        { MUSCLES::LEFT_TA, "LEFT_TA" },
        { MUSCLES::RIGHT_HF, "RIGHT_HF" },
        { MUSCLES::RIGHT_GLU, "RIGHT_GLU" },
        { MUSCLES::RIGHT_VAS, "RIGHT_VAS" },
        { MUSCLES::RIGHT_HAM, "RIGHT_HAM" },
        { MUSCLES::RIGHT_GAS, "RIGHT_GAS" },
        { MUSCLES::RIGHT_SOL, "RIGHT_SOL" },
        { MUSCLES::RIGHT_TA, "RIGHT_TA" },
    };*/

}



#endif