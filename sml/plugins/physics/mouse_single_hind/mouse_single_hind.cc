/*
 * File: mouse_single_hind.c
 * Date: 23.03.2017
 * Description: Physics plugin to fix the pelvis of the mouse hind limb to the world
 * Author: Shravan Tata Ramalingasetty
 * Modifications:
 */

#include <ode/ode.h>
#include <plugins/physics.h>
#include <string>

static dBodyID body_Pelvis;
static dBodyID body_Femur;

static dJointID joint_fixed;

static const dReal* body_Pelvis_pos;
static const dReal* body_Femur_pos; 

void webots_physics_step() {
body_Pelvis_pos = dBodyGetPosition(body_Pelvis);
body_Femur_pos = dBodyGetPosition(body_Femur);
	
	// --------------- MUSCLE 1 -------------------------------------------------
	// setup draw style
	glDisable(GL_LIGHTING);
	glLineWidth(10.0);

		// draw a red forward line
		glBegin(GL_LINES);
			glColor3f(1, 1, 0);
			glVertex3f(0,0,0);
			glVertex3f(0,-0.5,-0.65);
		glEnd();
		// draw a vertical line
		// glBegin(GL_LINES);
		// 	glColor3f(1, 0, 1);
		// 	glVertex3f(body_Regis_pos[0], body_Regis_pos[1] + draw_biais_Y                    , body_Regis_pos[2] + draw_biais_Z);
		// 	glVertex3f(body_Regis_pos[0], body_Regis_pos[1] + lift_force/1000.0 + draw_biais_Y, body_Regis_pos[2] + draw_biais_Z);
		// glEnd();


}

void webots_physics_init()
{
	dWebotsConsolePrintf("Secondary Constructor \n    In physic plug-in.\n");

	//find body in the scene tree
	body_Pelvis = dWebotsGetBodyFromDEF("pelvis");
	if (body_Pelvis == NULL)
	{	//we check we found the body
		dWebotsConsolePrintf("ERROR: could not find the body_Pelvis \n    In physic plug-in.\n");
		return;
	}

	//find body in the scene tree
	body_Femur = dWebotsGetBodyFromDEF("femur");
	if (body_Femur == NULL)
	{	//we check we found the body
		dWebotsConsolePrintf("ERROR: could not find the body_Femur \n    In physic plug-in.\n");
		return;
	}

	dWorldID world = dBodyGetWorld(body_Pelvis);

	// Create a fixed body attached to the world

	joint_fixed = dJointCreateFixed(world, 0); // Create a fixed joint with the world

	dJointAttach (joint_fixed, body_Pelvis, 0); // Fix the pelvis to the world

	dJointSetFixed (joint_fixed); // Initialize the joint
}


void webots_physics_draw() {

	body_Pelvis_pos = dBodyGetPosition (body_Pelvis);
	body_Femur_pos = dBodyGetPosition(body_Femur);
	
	

	// --------------- MUSCLE 1 -------------------------------------------------
	// setup draw style
	glDisable(GL_LIGHTING);
	glLineWidth(1000.0);
			dWebotsConsolePrintf("ERROR: could not find the body_Femur \n    In physic plug-in.\n");

		// draw a red forward line
		glBegin(GL_LINES);
			glColor3f(1, 0, 0);
			glVertex3f(body_Pelvis_pos[0],body_Pelvis_pos[1],body_Pelvis_pos[2]);
			glVertex3f(body_Femur_pos[0],body_Femur_pos[1],body_Femur_pos[2]);
		glEnd();
		// draw a vertical line
		// glBegin(GL_LINES);
		// 	glColor3f(1, 0, 1);
		// 	glVertex3f(body_Regis_pos[0], body_Regis_pos[1] + draw_biais_Y                    , body_Regis_pos[2] + draw_biais_Z);
		// 	glVertex3f(body_Regis_pos[0], body_Regis_pos[1] + lift_force/1000.0 + draw_biais_Y, body_Regis_pos[2] + draw_biais_Z);
		// glEnd();



}


int webots_physics_collide(dGeomID g1, dGeomID g2) {
	/*
	 * This function needs to be implemented if you want to overide Webots collision detection.
	 * It must return 1 if the collision was handled and 0 otherwise.
	 * Note that contact joints should be added to the contactJointGroup, e.g.
	 *   n = dCollide(g1, g2, MAX_CONTACTS, &contact[0].geom, sizeof(dContact));
	 *   ...
	 *   dJointCreateContact(world, contactJointGroup, &contact[i])
	 *   dJointAttach(contactJoint, body1, body2);
	 *   ...
	 */
	return 0;
}

void webots_physics_cleanup() {
	/*
	 * Here you need to free any memory you allocated in above, close files, etc.
	 * You do not need to free any ODE object, they will be freed by Webots.
	 */
}