/*

This file is created as interface with Webots and libnmm package.
Author : STata
Data : 21-11-2016

*/
#include <iostream>
#include <limits>
#include "gw_validation.hh"
#include <fstream>
#include <math.h>

using namespace std;
using namespace webots;


Muscle::Muscle() {


// Get the joints and position sensors

  joints[0] = getMotor("JOINT1");

  joints_pos[0] = getPositionSensor("JOINT1_POS");


  // ENABLE POSITION SENSOR
  joints_pos[0]->enable(TIME_STEP);



// TO Save Data
  save_data.open("/Volumes/tataramsdfdsa/Stata-PhD/Data/ROS/webots_validation_temp.txt");

        if (joint_data_file.is_open())
        {
            cout << "File Successfully read" << endl;
        }
        else
        {
            cerr << "Unable to open file : Terminating program" << endl;
            cerr << "Check if the file path exists" <<  endl;
        }

// Header
  save_data << "Time" << "\t" << "Joint1" << "\n" ;

}

Muscle::~Muscle() {

  save_data.close();
}

void Muscle::MuscleSimulate() {

  double time = 0;

  while (step(TIME_STEP) != -1) {

    //time = getTime();

    time += 1/100;

    double torque = 0;

    joints[0]->setTorque(torque);

    // Write Data
    save_data << time << "\t" << joints_pos[0]->getValue() << "\n";

    //std::cout << "Joint Pos : " << joints_pos[0]->getValue() << std::endl;
  }

}



int main(int argc, char **argv) {

  Muscle *Muscle1 = new Muscle();

  Muscle1->MuscleSimulate();

  return 0;

  delete Muscle1;
}