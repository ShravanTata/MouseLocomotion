#include "HindMouse.hh"
#include <sml/sml-tools/Settings.hh>
#include <webots/Node.hpp>
#include <webots/Receiver.hpp>
#include <webots/PositionSensor.hpp>
#include <webots/Motor.hpp>
#include <webots/Emitter.hpp>
#include <boost/xpressive/xpressive.hpp>
#include <cstdlib>

using namespace std;
using namespace webots;
using namespace boost::xpressive;
using namespace MOUSE;

extern double debug;



void HindMouse::ConstantInit() {
    // Joints constant
     sml::Constant[CONSTANT::REF_POS_HIP_LEFT] = 50 * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MAX_POS_HIP_LEFT] = +50.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MIN_POS_HIP_LEFT] = -50.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::REF_POS_KNEE_LEFT] = -150 * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MAX_POS_KNEE_LEFT] = -45.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MIN_POS_KNEE_LEFT] = -150.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT] = 30 * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MAX_POS_ANKLE_LEFT] = +50.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MIN_POS_ANKLE_LEFT] = -50.0  * (2.0*PI/360.0);

    sml::Constant[CONSTANT::REF_POS_HIP_RIGHT] = 50 * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MAX_POS_HIP_RIGHT] = +50.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MIN_POS_HIP_RIGHT] = -50.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::REF_POS_KNEE_RIGHT] = -150 * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MAX_POS_KNEE_RIGHT] = -45.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MIN_POS_KNEE_RIGHT] = -150.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::REF_POS_ANKLE_RIGHT] = 30 * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MAX_POS_ANKLE_RIGHT] = +50.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MIN_POS_ANKLE_RIGHT] = -50.0  * (2.0*PI/360.0);
    #ifdef MODEL3D
    sml::Constant[CONSTANT::REF_POS_HIPCOR_LEFT] = 0.0;
    sml::Constant[CONSTANT::MAX_POS_HIPCOR_LEFT] = 80.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MIN_POS_HIPCOR_LEFT] = 0.0  * (2.0*PI/360.0);
    #endif
     //PMA
    sml::Constant[CONSTANT::REF_PHI_PMA] =  18.68686 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_PMA] =  43.939393 * (2.0 * PI / 360.0) ;
    //CF
    sml::Constant[CONSTANT::REF_PHI_CF] = 13.6336 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_CF] = -16.6667 * (2.0 * PI / 360.0) ;
    //SM
    sml::Constant[CONSTANT::REF_PHI_SM_HIP] = 35.858585 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_SM_HIP] = 10.60606 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::REF_PHI_SM_KNEE] = -99.5959 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_SM_KNEE] = -95.20202 * (2.0 * PI / 360.0) ;
    //POP
    //CHANGE = -118.6364 -> -90
    sml::Constant[CONSTANT::REF_PHI_POP] =  -118.63636 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_POP] = -136.2121 * (2.0 * PI / 360.0) ;
    //RF
    sml::Constant[CONSTANT::REF_PHI_RF] =  -103.98989 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_RF] =  -29.29292 * (2.0 * PI / 360.0) ;
    //TA
    sml::Constant[CONSTANT::REF_PHI_TA] =  22.7273 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_TA] =  10.6061 * (2.0 * PI / 360.0) ;
    //SOL
    sml::Constant[CONSTANT::REF_PHI_SOL] =  -2.5253 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_SOL] =  2.5253 * (2.0 * PI / 360.0) ;
    //LG
    sml::Constant[CONSTANT::REF_PHI_LG_KNEE] = -38.0808 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_LG_KNEE] = -45.404 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::REF_PHI_LG_ANKLE] = 20.7071 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_LG_ANKLE] = -0.5050 * (2.0 * PI / 360.0) ;
}

void HindMouse::InputInit() {
    for (unsigned joint_pos = INPUT::FIRST_JOINT, joint = JOINT::FIRST_JOINT; joint_pos <= INPUT::FIRST_JOINT + 5; joint_pos++, joint++) {
        getPositionSensor(JOINT::toString(joint)+"_POS")->enable(time_step);
    }

    getTouchSensor("LEFT_TOUCH")->enable(time_step);
    getTouchSensor("RIGHT_TOUCH")->enable(time_step);

    // If inverse simulation then open the data file
    if (INVERSE_SIMULATION == 1) {
        joint_data_file.open("../../../Data/KinematicData/MOUSE_TREADMILL.txt");

        if(joint_data_file.is_open())
        {
            cout << "File Successfully read \n \n \n " << endl;
        }
        else
        {
            cerr << "File not successfully read. Please check if the file exists" << endl; 
        }

    }

    InputUpdate();

    /*    For the closedloop    */
    if (SPINAL_SIMULATION == 1) {
        dtCommunication = 0.020; //s
        tLastStep = 0;
        readData = true;
        initializeClientTCP();
    }
}


void HindMouse::InputUpdate() {
    for (unsigned joint_pos = INPUT::FIRST_JOINT, joint = JOINT::FIRST_JOINT; joint_pos <= INPUT::FIRST_JOINT + 5; joint_pos++, joint++) {
            sml::Input[joint_pos] = getPositionSensor(JOINT::toString(joint)+"_POS")->getValue() + sml::Constant[CONSTANT::FIRST_REF_POS_JOINT+joint_pos];
    }
}
void HindMouse::init() {
    supervisor::step(time_step);
    // Keyboard Initialize
    key.enable(time_step * 100);
    for (int j = 0; j < 16; j++) {
        muscleActivation[j] = 0.01;
    }

    if(LOG_DATA)
    {
    // Open file to log data
    logData.open("../../../results/LogData/InverseKinematics.txt");
    logData << "Time \t";

    for (auto& joint : joints)
        logData << joint->getName() << "\t";

    for (auto& muscle : muscles)
        logData << muscle->getName() << "\t";

    logData << "\n";

    time = 0.0;
    }
}

void HindMouse::step_SPINE_TO_MTU(double dt) {

    //LEFT LEG MUSCLE ACTIVATION
    muscles[0]->stim = muscleActivation[0];
    muscles[1]->stim = muscleActivation[1];
    muscles[2]->stim = muscleActivation[2];
    muscles[3]->stim = muscleActivation[3];
    muscles[4]->stim = muscleActivation[4];
    muscles[5]->stim = muscleActivation[5];
    muscles[6]->stim = muscleActivation[6];
    muscles[7]->stim = muscleActivation[7];

    //RIGHT LEG MUSCLE ACTIVATION
    muscles[8]->stim = muscleActivation[8];
    muscles[9]->stim = muscleActivation[9];
    muscles[10]->stim = muscleActivation[10];
    muscles[11]->stim = muscleActivation[11];
    muscles[12]->stim = muscleActivation[12];
    muscles[13]->stim = muscleActivation[13];
    muscles[14]->stim = muscleActivation[14];
    muscles[15]->stim = muscleActivation[15];


    if (SPINAL_SIMULATION == 1) {
        if (this->readData)
        {
            if (supervisor::getTime() < 0.02)
            {
                sendMusclesData();
            }
            // The reading is blocking
            this->message = readDataTCP();
            this->readData = false;
            cout << "integrating webots..." << endl;
        }
        for (auto line : this->message) {
            muscles[MUSCLES_MOUSE::toID(line[0])]->stim = atof(line[1].c_str());
        }
        if ((supervisor::getTime() - this->tLastStep) >= (this->dtCommunication - 0.0005))
        {
            sendMusclesData();
            tLastStep = supervisor::getTime();
            this->readData = true;
        }
    }

    // Keyboard control
    KeyboardMuscleActivation();
}


void HindMouse::KeyboardMuscleActivation() {

    // TO CHANGE :  ADD SWITCH CASE FOR LEFT AND RIGHT LEG CONTROL

    isKeyPress = false;

    int keyPress = key.getKey() + key.getKey();

    char muscleNumber[8] = {'1', '2', '3', '4', '5', '6', '7', '8'};

    for (int j = 0; j < 8; j++) {
        if (keyPress == 'U' + muscleNumber[j]) {
            muscleActivation[j] > 0.99 ? 1.0 : (muscleActivation[j] = muscleActivation[j] + 0.1);
            muscleActivation[j+8] = muscleActivation[j];
            isKeyPress = true;
            std::cout << " Muscle " << j << " : " << muscleActivation[j] << std::endl;
        }
        else if (keyPress == 'D' + muscleNumber[j]) {
            muscleActivation[j] <= 0.01 ? 0.01 : (muscleActivation[j] = muscleActivation[j] - 0.1);
            muscleActivation[j+8] = muscleActivation[j];
            isKeyPress = true;
            std::cout << " Muscle " << j << " : " << muscleActivation[j] << std::endl;
        }
    }
    if (keyPress == 'R' + '0') {
        for (int j = 0; j < 8; j++) {
            muscleActivation[j] = 0.01;
            muscleActivation[j+8] = muscleActivation[j];
            isKeyPress = true;
        }
    }
}


int HindMouse::step() {

    // Update time
    time += 0.001;

    // FOR DEBUG PURPOSE ONLY
    static int f = 1;
    if (f < 1)
    {
        Settings::printAll();
        f++;
    }

    sml::step();

    step_SPINE_TO_MTU(dt);

    if (INVERSE_SIMULATION)
    {
        getJointPosition();
    }

    for (unsigned joint = JOINT::FIRST_JOINT,
            joint_tau = OUTPUT::FIRST_JOINT;
            joint_tau <= OUTPUT::FIRST_JOINT + 5;
            joint_tau++,
            joint++
        )
    {
        double torque;
        
        torque = sml::Output[joint_tau];

        if (INVERSE_SIMULATION == 1)
        {
            getMotor(JOINT::toString(joint))->setPosition(joint_angles[joint]);
        }
        else
        {
            getMotor(JOINT::toString(joint))->setTorque(torque * 1e7);
        }
    }
    // getMotor("PELVIS_LEFT")->setPosition(joint_angles[6]);
    // getMotor("PELVIS_RIGHT")->setPosition(joint_angles[6]);

    // getMotor("FORE_U_LEFT")->setPosition(0);
    // getMotor("FORE_L_LEFT")->setPosition(0);
    // getMotor("FOREPAW_LEFT")->setTorque(0);
    // getMotor("FORE_U_RIGHT")->setPosition(0);
    // getMotor("FORE_L_RIGHT")->setPosition(0);
    // getMotor("FOREPAW_RIGHT")->setTorque(0);

    if (LOG_DATA)
    {
        this->StepLogData();
    }


    return 0;


}

void HindMouse::StepLogData()
{
    double const * translation = d_mouseTranslation->getSFVec3f();

    logData << time << "\t";

    //Mouse Position
    logData << translation[0] << "\t" << translation[1] << "\t" << translation[2] << "\t";

    for (auto& joint : joints)
    {   
        logData << joint->getAngle() * 180 / 3.14 << "\t";
    }

    for (auto&  muscle : muscles) 
    {
        logData << muscle->getA() << "\t";
    }

    logData << getTouchSensor("LEFT_TOUCH")->getValue() << "\t" << getTouchSensor("RIGHT_TOUCH")->getValue() << "\t";
    logData << "\n";
}

int HindMouse::getJointPosition() {
    //static int count = 0;
    // Function to return the joint position angles from the kinematic data
    for (unsigned int j = 0; j < 8; j++)
    {
        joint_data_file >> joint_angles[j];

        // cout << "Joint : " << j << " = " << joint_angles[j] << "\t";

    }

    // cout << endl;
    if (joint_data_file.eof())
    {
        std::cout << "END OF FILE :" << std::endl;
        return 1;
    }
    else
    {
        return 0;
    }

}

// Close-Loop communication
void HindMouse::sendMusclesData() {
    // LEFT LEG
    // double LEFT_PMA_stretch = muscles[MUSCLES_MOUSE::toID("LEFT_PMA")]->getL_CE() -
    // muscles[MUSCLES_MOUSE::toID("LEFT_PMA")]->l_opt;
    // double LEFT_CF_stretch = muscles[MUSCLES_MOUSE::toID("LEFT_CF")]->getL_CE() -
    // muscles[MUSCLES_MOUSE::toID("LEFT_CF")]->l_opt;
    // double LEFT_SM_stretch = muscles[MUSCLES_MOUSE::toID("LEFT_SM")]->getL_CE() -
    // muscles[MUSCLES_MOUSE::toID("LEFT_SM")]->l_opt;
    // double LEFT_POP_stretch = muscles[MUSCLES_MOUSE::toID("LEFT_POP")]->getL_CE() -
    // muscles[MUSCLES_MOUSE::toID("LEFT_POP")]->l_opt;
    // double LEFT_RF_stretch = muscles[MUSCLES_MOUSE::toID("LEFT_RF")]->getL_CE() -
    // muscles[MUSCLES_MOUSE::toID("LEFT_RF")]->l_opt;
    double LEFT_TA_stretch = muscles[MUSCLES_MOUSE::toID("LEFT_TA")]->getL_CE() -
                             muscles[MUSCLES_MOUSE::toID("LEFT_TA")]->l_opt;
    // double LEFT_SOL_stretch = muscles[MUSCLES_MOUSE::toID("LEFT_SOL")]->getL_CE() -
    // muscles[MUSCLES_MOUSE::toID("LEFT_SOL")]->l_opt;
    double LEFT_LG_stretch = muscles[MUSCLES_MOUSE::toID("LEFT_LG")]->getL_CE() -
                             muscles[MUSCLES_MOUSE::toID("LEFT_LG")]->l_opt;

    // RIGHT LEG
    // double RIGHT_PMA_stretch = muscles[MUSCLES_MOUSE::toID("RIGHT_PMA")]->getL_CE() -
    // muscles[MUSCLES_MOUSE::toID("RIGHT_PMA")]->l_opt;
    // double RIGHT_CF_stretch = muscles[MUSCLES_MOUSE::toID("RIGHT_CF")]->getL_CE() -
    // muscles[MUSCLES_MOUSE::toID("RIGHT_CF")]->l_opt;
    // double RIGHT_SM_stretch = muscles[MUSCLES_MOUSE::toID("RIGHT_SM")]->getL_CE() -
    // muscles[MUSCLES_MOUSE::toID("RIGHT_SM")]->l_opt;
    // double RIGHT_POP_stretch = muscles[MUSCLES_MOUSE::toID("RIGHT_POP")]->getL_CE() -
    // muscles[MUSCLES_MOUSE::toID("RIGHT_POP")]->l_opt;
    // double RIGHT_RF_stretch = muscles[MUSCLES_MOUSE::toID("RIGHT_RF")]->getL_CE() -
    // muscles[MUSCLES_MOUSE::toID("RIGHT_RF")]->l_opt;
    double RIGHT_TA_stretch = muscles[MUSCLES_MOUSE::toID("RIGHT_TA")]->getL_CE() -
                              muscles[MUSCLES_MOUSE::toID("RIGHT_TA")]->l_opt;
    // double RIGHT_SOL_stretch = muscles[MUSCLES_MOUSE::toID("RIGHT_SOL")]->getL_CE() -
    // muscles[MUSCLES_MOUSE::toID("RIGHT_SOL")]->l_opt;
    double RIGHT_LG_stretch = muscles[MUSCLES_MOUSE::toID("RIGHT_LG")]->getL_CE() -
                              muscles[MUSCLES_MOUSE::toID("RIGHT_LG")]->l_opt;

    // dof net stretch
    cout << "COMM_OUT" << endl;
    cout << 1000 * supervisor::getTime() << endl;
    // cout << "LEFT_PMA " << LEFT_PMA_stretch << endl;
    // cout << "LEFT_CF " << LEFT_CF_stretch << endl;
    // cout << "LEFT_SM " << LEFT_SM_stretch << endl;
    // cout << "LEFT_POP " << LEFT_POP_stretch << endl;
    // cout << "LEFT_RF " << LEFT_RF_stretch << endl;
    cout << "LEFT_TA " << LEFT_TA_stretch << endl;
    // cout << "LEFT_SOL " << LEFT_SOL_stretch << endl;
    cout << "LEFT_LG " << LEFT_LG_stretch << endl;

    // cout << "RIGHT_PMA " << RIGHT_PMA_stretch << endl;
    // cout << "RIGHT_CF " << RIGHT_CF_stretch << endl;
    // cout << "RIGHT_SM " << RIGHT_SM_stretch << endl;
    // cout << "RIGHT_POP " << RIGHT_POP_stretch << endl;
    // cout << "RIGHT_RF " << RIGHT_RF_stretch << endl;
    cout << "RIGHT_TA " << RIGHT_TA_stretch << endl;
    // cout << "RIGHT_SOL " << RIGHT_SOL_stretch << endl;
    cout << "RIGHT_LG " << RIGHT_LG_stretch << endl;

    cout << "END" << endl;
}

void HindMouse::initializeClientTCP() {
    int portno;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    portno = 5005;
    this->sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (this->sockfd < 0)
    {
        fprintf(stderr, "ERROR opening socket");
        exit(0);
    }
    server = gethostbyname("127.0.0.1");
    if (server == NULL)
    {
        fprintf(stderr, "ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
          (char *)&serv_addr.sin_addr.s_addr,
          server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(this->sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
    {
        fprintf(stderr, "ERROR connecting");
        exit(0);
    }
}


msgList HindMouse::readDataTCP() {
    int n;
    bzero(this->buffer, 512);
    n = read(this->sockfd, this->buffer, 511);
    if (n < 0) {
        fprintf(stdout, "ERROR reading from socket");
        exit(0);
    }

    string tempMessage = string(this->buffer);
    std::vector<std::string> lines;
    boost::split(lines, tempMessage, boost::is_any_of("\n"), boost::token_compress_on);

    msgList message;
    std::vector<std::string> words;
    for (auto line : lines) {
        boost::split(words, line, boost::is_any_of(" "), boost::token_compress_on);
        if (words[0].compare("END") == 0)
            break;
        message.push_back(words);
    }
    return message;
}

HindMouse::~HindMouse() {
    close(this->sockfd);
    if(LOG_DATA)
    {
    logData.close();
    }
}
