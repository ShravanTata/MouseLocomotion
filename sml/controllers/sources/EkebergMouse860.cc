#include "EkebergMouse860.hh"
#include <sml/sml-tools/Settings.hh>
#include <webots/Node.hpp>
#include <webots/Receiver.hpp>
#include <webots/PositionSensor.hpp>
#include <webots/Motor.hpp>
#include <webots/Emitter.hpp>
#include <boost/xpressive/xpressive.hpp>
#include <cstdlib>

using namespace std;
using namespace webots;
using namespace boost::xpressive;
using namespace MOUSE;

extern double debug;

void EkebergMouse860::ConstantInit() {
    // Joints constant
    sml::Constant[CONSTANT::REF_POS_HIP_LEFT] = 30 * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MAX_POS_HIP_LEFT] = +50.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MIN_POS_HIP_LEFT] = -50.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::REF_POS_KNEE_LEFT] = -100 * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MAX_POS_KNEE_LEFT] = -45.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MIN_POS_KNEE_LEFT] = -150.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT] = 30 * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MAX_POS_ANKLE_LEFT] = +50.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MIN_POS_ANKLE_LEFT] = -50.0  * (2.0*PI/360.0);

    sml::Constant[CONSTANT::REF_POS_HIP_RIGHT] = 30 * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MAX_POS_HIP_RIGHT] = +50.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MIN_POS_HIP_RIGHT] = -50.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::REF_POS_KNEE_RIGHT] = -100 * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MAX_POS_KNEE_RIGHT] = -45.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MIN_POS_KNEE_RIGHT] = -150.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::REF_POS_ANKLE_RIGHT] = 30 * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MAX_POS_ANKLE_RIGHT] = +50.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MIN_POS_ANKLE_RIGHT] = -50.0  * (2.0*PI/360.0);

    #ifdef MODEL3D
    sml::Constant[CONSTANT::REF_POS_HIPCOR_LEFT] = 0.0;
    sml::Constant[CONSTANT::MAX_POS_HIPCOR_LEFT] = 80.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MIN_POS_HIPCOR_LEFT] = 0.0  * (2.0*PI/360.0);
    #endif
     //PMA
    sml::Constant[CONSTANT::REF_PHI_PMA] =  18.68686 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_PMA] =  43.939393 * (2.0 * PI / 360.0) ;
    //CF
    sml::Constant[CONSTANT::REF_PHI_CF] = 13.6336 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_CF] = -16.6667 * (2.0 * PI / 360.0) ;
    //SM
    sml::Constant[CONSTANT::REF_PHI_SM_HIP] = 35.858585 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_SM_HIP] = 10.60606 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::REF_PHI_SM_KNEE] = -99.5959 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_SM_KNEE] = -95.20202 * (2.0 * PI / 360.0) ;
    //POP
    //CHANGE = -118.6364 -> -90
    sml::Constant[CONSTANT::REF_PHI_POP] =  -118.63636 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_POP] = -136.2121 * (2.0 * PI / 360.0) ;
    //RF
    sml::Constant[CONSTANT::REF_PHI_RF] =  -103.98989 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_RF] =  -29.29292 * (2.0 * PI / 360.0) ;
    //TA
    sml::Constant[CONSTANT::REF_PHI_TA] =  22.7273 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_TA] =  10.6061 * (2.0 * PI / 360.0) ;
    //SOL
    sml::Constant[CONSTANT::REF_PHI_SOL] =  -2.5253 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_SOL] =  2.5253 * (2.0 * PI / 360.0) ;
    //LG
    sml::Constant[CONSTANT::REF_PHI_LG_KNEE] = -38.0808 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_LG_KNEE] = -45.404 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::REF_PHI_LG_ANKLE] = 20.7071 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_LG_ANKLE] = -0.5050 * (2.0 * PI / 360.0) ;
}

void EkebergMouse860::InputInit() {
    for (unsigned joint_pos=INPUT::FIRST_JOINT, joint=JOINT::FIRST_JOINT; joint_pos<=INPUT::FIRST_JOINT + NUM_JOINTS; joint_pos++, joint++)
    {
        getPositionSensor(JOINT::toString(joint)+"_POS")->enable(time_step);
    }
    getTouchSensor("LEFT_TOUCH")->enable(time_step);
    getTouchSensor("RIGHT_TOUCH")->enable(time_step);
    InputUpdate();
}

void EkebergMouse860::InputUpdate() {
   for (unsigned joint_pos=INPUT::FIRST_JOINT, joint=JOINT::FIRST_JOINT; joint_pos<=INPUT::FIRST_JOINT + NUM_JOINTS; joint_pos++, joint++)
   {
    sml::Input[joint_pos] = getPositionSensor(JOINT::toString(joint)+"_POS")->getValue() + sml::Constant[CONSTANT::FIRST_REF_POS_JOINT+joint_pos];
   }
}

void EkebergMouse860::init() {
    supervisor::step(time_step);
    key.enable(time_step * 100);
    control  = new EkerbergControl();
    if(DEBUG)
    {
        cout << "[ok] : Mouse init complete << endl";
    }

    if(LOG_DATA)
    {
    // Open file to log data
    logData.open("../../../results/LogData/Results_Ekeberg.txt");
    logData << "Time \t";

    for (auto& joint : joints)
        logData << joint->getName() << "\t";

    for (auto& muscle : muscles)
        logData << muscle->getName() << "\t";

    logData << "\n";

    time = 0.0;
    }

}

void EkebergMouse860::step_SPINE_TO_MTU(double dt) 
{
    time += 0.001;
    //Get Muscle Forces
    for (uint j = 0; j < 16; j ++)
    {
        muscleForces[j] = muscles[j]->getForce();
    }
    if(time > 1.0 && (state))
    {
    
    control->Step(sml::Input,muscleForces,getTouchSensor("LEFT_TOUCH")->getValue(), getTouchSensor("RIGHT_TOUCH")->getValue());
    // this->KeyboardActivation();
    // control->DebugStates(sml::Input,muscleForces,getTouchSensor("LEFT_TOUCH")->getValue(), getTouchSensor("RIGHT_TOUCH")->getValue(),state_left,state_right);
    muscleActivations = control->MuscleActivations();
    
    }
    else if(!state)
    {
        // control->DebugStates(sml::Input,muscleForces,getTouchSensor("LEFT_TOUCH")->getValue(), getTouchSensor("RIGHT_TOUCH")->getValue(),3,3);
    }
    this->KeyboardActivation();

    // Stimulate the muscles of hind limbs
    for (int j = 0; j < 16; j++)
    {
        muscles[j]->stim = muscleActivations[j];
    }
}


void EkebergMouse860::KeyboardActivation() {

    // TO CHANGE :  ADD SWITCH CASE FOR LEFT AND RIGHT LEG CONTROL

    int keyPress = key.getKey() ;

        if (keyPress == 'R') {
            state = true;
            cout << "State Walking : " << endl;   
        }
        else if (keyPress == 'S') {
            state = false;
            cout << "State Pause : "  << endl;   
        }
}


int EkebergMouse860::step() {
    // FOR DEBUG PURPOSE ONLY
    static int f = 1;
    if (f < 1)
    {
        Settings::printAll();
        f++;
    }

    sml::step();

    step_SPINE_TO_MTU(dt);

    for (unsigned joint = JOINT::FIRST_JOINT,
        joint_tau = OUTPUT::FIRST_JOINT;
        joint_tau <= OUTPUT::FIRST_JOINT + NUM_JOINTS;
        joint_tau++,
        joint++
        )
    {
        double torque;

        torque = sml::Output[joint_tau];

        getMotor(JOINT::toString(joint))->setTorque(torque * 1e7);
    }
    // getMotor("SCAPULA_LEFT")->setPosition(0);
    // getMotor("SCAPULA_RIGHT")->setPosition(0);

    // getMotor("FORE_U_LEFT")->setPosition(0);
    // getMotor("FORE_L_LEFT")->setPosition(0);
    getMotor("FOREPAW_LEFT")->setTorque(0);
    // getMotor("FORE_U_RIGHT")->setPosition(0);
    // getMotor("FORE_L_RIGHT")->setPosition(0);
    getMotor("FOREPAW_RIGHT")->setTorque(0);

    if (LOG_DATA)
    {
        this->StepLogData();
    }

    return 0;
}

void EkebergMouse860::StepLogData()
{

    double const * translation = d_mouseTranslation->getSFVec3f();

    logData << time << "\t";

    //Mouse Position
    logData << translation[0] << "\t" << translation[1] << "\t" << translation[2] << "\t";

    for (auto& joint : joints)
    {   
        logData << joint->getAngle() * 180 / 3.14 << "\t";
    }

    for (auto&  muscle : muscles) 
    {
        logData << muscle->getA() << "\t";
    }

    logData << getTouchSensor("LEFT_TOUCH")->getValue() << "\t" << getTouchSensor("RIGHT_TOUCH")->getValue() << "\t";
    logData << control->current_left_state << "\t" << control->current_right_state << "\t" << endl;

    logData << "\n";
}

EkebergMouse860::~EkebergMouse860() 
{
    if(LOG_DATA)
    {
    logData.close();
    }
}
