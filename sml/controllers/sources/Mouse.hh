#ifndef __Mouse_HH__
#define __Mouse_HH__


#include <string>
#include <math.h>
#include <vector>
#include <fstream>
#include <map>
#include <limits>
#include <iostream>
#include <webots/Keyboard.hpp>
#include <webots/Servo.hpp>
#include <webots/Supervisor.hpp>
#include <webots/TouchSensor.hpp>
#include <sml/types/types.h>
#include <sml/musculoSkeletalSystem/SmlMouseSingle.hh>

#define DEBUG 0
#define LOG_DATA 1
#define NUM_JOINTS 2
#define NUM_MUSCLES 8
#define INVERSE_SIMULATION 1

class MouseSingle: public webots::Supervisor, public SmlMouseSingle
{
private:

    void ConstantInit();
    void InputInit();
    void InputUpdate();
    void step_SPINE_TO_MTU(double dt);
    int getJointPosition();
    void KeyboardMuscleActivation();
    webots::Keyboard key;
    float muscleActivation[NUM_MUSCLES];

    ~MouseSingle();

    ofstream logData;
    void StepLogData();

public:
    typedef Supervisor supervisor;
    typedef SmlMouseSingle sml;
    int step();
   
  //-----------------------------------

    //constructor
    MouseSingle(): SmlMouseSingle(){
        sml::init();
        init();
        }

    //destructor
    //~Rob();
    void init();
    // Variables
    std::ifstream joint_data_file;
    double joint_angles[NUM_JOINTS+1];
};

#endif /* __MOUSE_HH__ */
