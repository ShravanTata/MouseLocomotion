#ifndef __HindMouse_HH__
#define __HindMouse_HH__

#include <string>
#include <math.h>
#include <vector>
#include <fstream>
#include <map>
#include <limits>
#include <iostream>
//#include <boost/algorithm/string.hpp>
#include <netinet/in.h>
#include <sys/types.h>
#include <netdb.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <webots/Keyboard.hpp>
#include <webots/Servo.hpp>
#include <webots/Supervisor.hpp>
#include <webots/TouchSensor.hpp>
#include <webots/Display.hpp>
#include <sml/types/types.h>
#include <sml/musculoSkeletalSystem/SmlMouse.hh>

#define DEBUG 0
#define INVERSE_SIMULATION 1
#define SPINAL_SIMULATION 0
#define LOG_DATA 1

typedef struct{
  int is_in_lift_mode;
  double last_force_y;
  double last_force_z;
  double last_force_pos;
} PtoW; //from Physic plugin to Webots

typedef struct{
//  int reinitialisation;
  int backward;
  double force_amplitude;
} WtoP; //from Webots to Physic plug in

typedef struct{
  double muscle_signal;
  double muscle_activity;
} WtoM; //from Webots to Raw supervisor

typedef std::vector<std::vector<std::string>> msgList;

class HindMouse: public webots::Supervisor, public SmlMouse
{
private:

    void ConstantInit();
    void InputInit();
    void InputUpdate();
    void step_SPINE_TO_MTU(double dt);
    int getJointPosition();
    void KeyboardMuscleActivation();
    webots::Keyboard key;
    float muscleActivation[16];
    bool isKeyPress;
    webots::Field *d_mouseTranslation;
    ofstream logData;
    void StepLogData();
    double time;


    void initializeClientTCP();
    msgList readDataTCP();
    void sendMusclesData();

    msgList message;
    char buffer[512];
    int sockfd;
    double dtCommunication;
    double tLastStep;
    bool readData;
    ~HindMouse();

    std::vector<double> muscleForces;

public:
    typedef Supervisor supervisor;
    typedef SmlMouse sml;
    int step();
    const int numJoints = 0;
  //-----------------------------------

    //constructor
    HindMouse(): SmlMouse(), muscleForces(16,0.0){
        sml::init();
        init();
        webots::Node *mouseNode = supervisor::getFromDef("MOUSE");
        d_mouseTranslation = mouseNode->getField("translation");
        }

    //destructor
    //~Rob();
    void init();
    // Variables
    std::ifstream joint_data_file;
    double joint_angles[8];
};

#endif /* __HINDMOUSE_HH__ */
