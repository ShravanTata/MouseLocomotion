#include "Mouse.hh"
#include <sml/sml-tools/Settings.hh>
#include <webots/Node.hpp>
#include <webots/Receiver.hpp>
#include <webots/PositionSensor.hpp>
#include <webots/Motor.hpp>
#include <webots/Emitter.hpp>
#include <boost/xpressive/xpressive.hpp>
#include <cstdlib>

using namespace std;
using namespace webots;
using namespace boost::xpressive;
using namespace MOUSE;

extern double debug;

void MouseSingle::ConstantInit(){

    // Joints constant
    sml::Constant[CONSTANT::REF_POS_HIP_LEFT] = 0 * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MAX_POS_HIP_LEFT] = +50.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MIN_POS_HIP_LEFT] = -50.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::REF_POS_KNEE_LEFT] = -100 * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MAX_POS_KNEE_LEFT] = -45.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MIN_POS_KNEE_LEFT] = -145.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT] = 0 * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MAX_POS_ANKLE_LEFT] = +50.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MIN_POS_ANKLE_LEFT] = -50.0  * (2.0*PI/360.0);
    #ifdef MODEL3D
    sml::Constant[CONSTANT::REF_POS_HIPCOR_LEFT] = 0.0;
    sml::Constant[CONSTANT::MAX_POS_HIPCOR_LEFT] = 80.0  * (2.0*PI/360.0);
    sml::Constant[CONSTANT::MIN_POS_HIPCOR_LEFT] = 0.0  * (2.0*PI/360.0);
    #endif
     //PMA
    sml::Constant[CONSTANT::REF_PHI_PMA] =  18.68686 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_PMA] =  43.939393 * (2.0 * PI / 360.0) ;
    //CF
    sml::Constant[CONSTANT::REF_PHI_CF] = 13.6336 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_CF] = -16.6667 * (2.0 * PI / 360.0) ;
    //SM
    sml::Constant[CONSTANT::REF_PHI_SM_HIP] = 35.858585 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_SM_HIP] = 10.60606 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::REF_PHI_SM_KNEE] = -99.5959 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_SM_KNEE] = -95.20202 * (2.0 * PI / 360.0) ;
    //POP
    //CHANGE = -118.6364 -> -90
    sml::Constant[CONSTANT::REF_PHI_POP] =  -118.63636 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_POP] = -136.2121 * (2.0 * PI / 360.0) ;
    //RF
    sml::Constant[CONSTANT::REF_PHI_RF] =  -103.98989 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_RF] =  -29.29292 * (2.0 * PI / 360.0) ;
    //TA
    sml::Constant[CONSTANT::REF_PHI_TA] =  22.7273 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_TA] =  10.6061 * (2.0 * PI / 360.0) ;
    //SOL
    sml::Constant[CONSTANT::REF_PHI_SOL] =  -2.5253 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_SOL] =  2.5253 * (2.0 * PI / 360.0) ;
    //LG
    sml::Constant[CONSTANT::REF_PHI_LG_KNEE] = -38.0808 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_LG_KNEE] = -45.404 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::REF_PHI_LG_ANKLE] = 20.7071 * (2.0 * PI / 360.0) ;
    sml::Constant[CONSTANT::MAX_PHI_LG_ANKLE] = -0.5050 * (2.0 * PI / 360.0) ;
    if(DEBUG)
    {
        cout << "[ok] Sml : Constant Init Done " << endl;
    }
}

void MouseSingle::InputInit(){
    for (unsigned joint_pos=INPUT::FIRST_JOINT, joint=JOINT::FIRST_JOINT; joint_pos<=INPUT::FIRST_JOINT + NUM_JOINTS; joint_pos++, joint++)
    {
        getPositionSensor(JOINT::toString(joint)+"_POS")->enable(time_step);
    }
    
    // If forward simulation then open the data file
    if(INVERSE_SIMULATION == 1)
    {
        joint_data_file.open("../../../Data/KinematicData/d_jointLimits.txt");
        if (joint_data_file.is_open())
        {
            cout << "File Successfully read" << endl;
        }
        else
        {
            cerr << "Enable to read file : Terminating program" << endl;
            cerr << "Check if the file exists in the path. " <<  endl;
        }
    }

    if(DEBUG)
    {
        cout << "[ok] Sml : Input Init Done " << endl;
    }

    InputUpdate();
}

void MouseSingle::InputUpdate(){
    for (unsigned joint_pos=INPUT::FIRST_JOINT, joint=JOINT::FIRST_JOINT; joint_pos<=INPUT::FIRST_JOINT + NUM_JOINTS; joint_pos++, joint++)
    {
        if(INVERSE_SIMULATION == 1)
        {
            sml::Input[joint_pos] = joint_angles[joint_pos] + sml::Constant[CONSTANT::FIRST_REF_POS_JOINT+joint_pos];
        }
        else
        {
            std::string ref_joint = "REF_POS_"+JOINT::toString(joint);
            sml::Input[joint_pos] = getPositionSensor(JOINT::toString(joint)+"_POS")->getValue() + sml::Constant[CONSTANT::FIRST_REF_POS_JOINT+joint_pos];
        }
    }
    if(DEBUG)
    {
        cout << "[ok] Sml : Input Update" << endl;
    }
}

void MouseSingle::init(){
    supervisor::step(time_step);
    // Keyboard Initialize
    key.enable(time_step*100);
    for(int j = 0; j < 8; j++){
        muscleActivation[j] = 0.01;
    }

    if(DEBUG)
    {
        cout << "[ok] Sml : Init Done " << endl;
    }

    if(LOG_DATA)
    {
    // Open file to log data
        logData.open("../../../results/LogData/Results_Inverse.txt");
        logData << "Time" << "\t";

        for (auto& joint : joints)
            logData << joint->getName() << "\t";

        for (auto& muscle : muscles)
            logData << muscle->getName() << "\t";

        logData << "\n";
    }
}

void MouseSingle::step_SPINE_TO_MTU(double dt){

    for (uint j = 0; j < NUM_MUSCLES; j++)
    {
        muscles[j]->stim = muscleActivation[j];
    }
    // Keyboard controll
    KeyboardMuscleActivation();
}

void MouseSingle::KeyboardMuscleActivation(){
    int keyPress = key.getKey() + key.getKey();
    char muscleNumber[8] = {'1','2','3','4','5','6','7','8'};
    for(int j = 0; j < 8; j++){
        if(keyPress == 'U' + muscleNumber[j]){
            muscleActivation[j] > 0.99 ? 1.0 : (muscleActivation[j] = muscleActivation[j] + 0.05);
            std::cout << " Muscle " << j << " : "<< muscleActivation[j] << std::endl;
            std::cout << muscles[j]->getName() << " Force : " << muscles[j]->getForce() << std::endl;
        }
        else if(keyPress == 'D' + muscleNumber[j]){
            muscleActivation[j] <= 0.01 ? 0.01 : (muscleActivation[j] = muscleActivation[j] - 0.05);
            std::cout << " Muscle " << j << " : "<< muscleActivation[j] << std::endl;
            std::cout << muscles[j]->getName() << " Force : " << muscles[j]->getForce() << std::endl;
        }
    }
    if (keyPress == 'R' + '0'){
        for(int j = 0; j < 8; j++){
            muscleActivation[j] = 0.01;
        }
    }
}

int MouseSingle::step(){

    // FOR DEBUG PURPOSE ONLY
    static int f=1;
    if(f<1){
        Settings::printAll();
        f++;
    }
    sml::step();
    step_SPINE_TO_MTU(dt);
    
    if(INVERSE_SIMULATION)
    {
        int checkState = getJointPosition();
        if(checkState)
        {
            return 1;
        }
    }
    for (unsigned joint=JOINT::FIRST_JOINT, joint_tau=OUTPUT::FIRST_JOINT; joint_tau<=OUTPUT::FIRST_JOINT + NUM_JOINTS; joint_tau++, joint++)
    {
        double torque;

        torque = sml::Output[joint_tau];

        if (INVERSE_SIMULATION == 1)
        {
            getMotor(JOINT::toString(joint))->setPosition(joint_angles[joint]);
        }
        else
        {
            getMotor(JOINT::toString(joint))->setTorque(torque*1e7);
        }
    }

    if (LOG_DATA)
    {
        this->StepLogData();
    }

    return 0;
}

void MouseSingle::StepLogData()
{


    static double time = 0;
    time += 0.001;


    cout << time << endl;


    logData << time << "\t";

    for (auto& joint : joints)
    {   
        logData << joint->getAngle() * 180 / 3.14 << "\t";
    }

    logData << muscles[0]->getForce() << "\t";
    logData << muscles[0]->getL_CE() << "\t";
    logData << muscles[0]->getL_MTC() << "\t";

    logData << "\n";
}

int MouseSingle::getJointPosition(){
    //static int count = 0; 
    for (unsigned int j = 0; j < NUM_JOINTS + 1; j++){
        joint_data_file >> joint_angles[j];
        joint_angles[j] *= 0.0;
    }
    if (joint_data_file.eof()){
        std::cout << "END OF FILE :" << std::endl;
        std::exit(EXIT_SUCCESS);
        return 1;
    }
    else
        return 0;
}


MouseSingle::~MouseSingle(){
    joint_data_file.close();
}
