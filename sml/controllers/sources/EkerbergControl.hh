#ifndef __EKERBERG_CONTROL_HH__
#define __EKERBERG_CONTROL_HH__

#include <iostream>
#include <vector>
#include <sml/types/types.h>
#include <string>

#define CONTROL_DEBUG 0

class EkerbergControl
{

private:
	// Variables
	std::vector<double> muscleActivations;
	std::vector<double> activations;
	std::vector<EKERBERG_PHASE::Phase> legCurrentPhase;
	std::vector<EKERBERG_PHASE::Phase> legPrevPhase;

	const bool crossCoupling = true;
	SIDE::Side contraLateralSide;
	SIDE::Side side;
	std::vector<double> angles;
	std::vector<double> forces;

	// Functions 
	void NeuralControl(bool groundContact_LEFT,SIDE::Side side);
	void Stance();
	void LiftOff();
	void Swing();
	void TouchDown();
	
	void PrintDebug(std::string t);

public:
	EkerbergControl() ;
	void Init();
	void Step(std::vector<double>& jointAngles, std::vector<double>& muscleForces, bool groundContact_LEFT,bool groundContact_RIGHT);
	void DebugStates(std::vector<double>& jointAngles, std::vector<double>& muscleForces, bool groundContact_LEFT,bool groundContact_RIGHT, uint state_left, uint state_right);
	
	std::vector<double> MuscleActivations();
	uint current_left_state;
    uint current_right_state;
};

#endif /* __EKERBERG_CONTROL_HH__ */