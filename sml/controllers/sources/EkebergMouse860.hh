#ifndef __EkebergMouse860_HH__
#define __EkebergMouse860_HH__

#include <string>
#include <math.h>
#include <vector>
#include <fstream>
#include <map>
#include <limits>
#include <iostream>
//#include <boost/algorithm/string.hpp>
#include <webots/Keyboard.hpp>
#include <webots/Servo.hpp>
#include <webots/Supervisor.hpp>
#include <webots/TouchSensor.hpp>
#include <webots/Display.hpp>
#include <sml/types/types.h>
#include <sml/musculoSkeletalSystem/SmlMouse.hh>
#include "EkerbergControl.hh"

#define DEBUG 0
#define NUM_JOINTS 5
#define LOG_DATA 1

class EkebergMouse860: public webots::Supervisor, public SmlMouse
{
private:

    void ConstantInit();
    void InputInit();
    void InputUpdate();
    void step_SPINE_TO_MTU(double dt);
    int getJointPosition();
    void KeyboardActivation();
    void StepLogData();

    ~EkebergMouse860();

    std::vector<double> muscleForces;
    std::vector<double> muscleActivations;
    webots::Keyboard key;
    EkerbergControl *control;

    bool state;
    webots::Field *d_mouseTranslation;
    ofstream logData;
    double time;


public:
    typedef Supervisor supervisor;
    typedef SmlMouse sml;
    int step();
  //-----------------------------------

    //constructor
    EkebergMouse860(): 
    muscleForces(16,0.0),
    muscleActivations(16,0.01),
    SmlMouse(),
    state(false){
        sml::init();
        init();
        webots::Node *mouseNode = supervisor::getFromDef("MOUSE");
        d_mouseTranslation = mouseNode->getField("translation");
        }

    //destructor
    //~Rob();
    void init();
    // Variables
};

#endif /* __EkebergMouse860_HH__ */
