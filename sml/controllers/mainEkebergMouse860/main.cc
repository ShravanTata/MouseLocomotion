#include <stdio.h>
#include <sml/sml.hh>

#include "../sources/EkebergMouse860.hh"
#include <string>
#include <vector>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>




using namespace std;

CentralClock * centralclock;
double debug=false;
std::map<std::string, double_int_string> Settings::sets;

int main(int argc, char* argv[])
{
    
    /**__**/ cout << "1) Loading settings...............";

	if(argc >= 2)
		Settings::filename = argv[1];
	if(argc >= 3){
		Settings::prefix = "_";
		Settings::prefix += argv[2];
	}
	cout << Settings::prefix << endl;
	Settings settings;
	if(argc >= 4){
		Settings::set<std::string>("robot_name", argv[3]);
	}
    
	srand(time(0));
	
	
 //    /**__**/ cout << "3) Create CentralClock...............";
	// centralclock = new CentralClock(parameters[1]["freq_change"]);
	/**__**/ cout << "Ok" << endl;
    /**__**/ cout << "2) Create Robot...............";
    EkebergMouse860 *MrGinggle = new EkebergMouse860();
    
    webots::Supervisor* supervisor = MrGinggle;
    //eventManager = Regis->eventManager;
    /**__**/ cout << "Ok" << endl;

	while(true) {
		MrGinggle->step();
		supervisor->step(Settings::get<int>("time_step"));
	}  

	return 0;
}
