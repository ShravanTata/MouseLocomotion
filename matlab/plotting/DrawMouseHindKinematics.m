%% To plot the hind kinematic data movement
function []= DrawMouseHindKinematics(time,hip,knee,ankle,pos,muscle_act,units)

if(nargin < 7)
    units = 'deg';
    disp('Assuming input angles in degree');
end

if(strcmp(units,'deg'))
    hip = deg2rad(hip);
    knee = deg2rad(knee);
    ankle = deg2rad(ankle);
end

% Time
time = 0 : 1/1000 : (length(hip)-1)/1000;


RotMat =@(phi)[cos(phi) sin(phi) 0;
               -sin(phi) cos(phi) 0;
               0        0         1];
    
femur_start = [0 0 0].';
femur_end = [0 -1 0].';

shank_start = [0 -1 0].';
shank_end = [0 -1.8 0].';

foot_start = [0 1.8 0].';
foot_end = [0 -2.55 0].';


% Rotate pelvis
hip = hip + deg2rad(0);

% Ankle offset
ankle = ankle + deg2rad(90);

%% Compute the data
femur_start = zeros(3,length(hip));
femur_end = zeros(3,length(hip));
shank_start = zeros(3,length(hip));
shank_end = zeros(3,length(hip));
foot_start = zeros(3,length(hip));
foot_end = zeros(3,length(hip));

for j = 1 : length(hip);

    femur_start(:,j) = RotMat(hip(j))*[0 0 0].';
    femur_end(:,j) = RotMat(hip(j))*[0 -1.2986 0].';

    femur_start(:,j) = femur_start(:,j) + pos(j,:).';
    femur_end(:,j) = femur_end(:,j) + pos(j,:).'; 

    shank_end(:,j) = RotMat(hip(j))*RotMat(knee(j))*[0 -1.7007 0].' + femur_end(:,j);

    foot_end(:,j) = RotMat(hip(j))*RotMat(knee(j))*RotMat(ankle(j))*[0 1.2 0].' + shank_end(:,j);
end
    shank_start = femur_end;
    foot_start = shank_end;


%% Plotting

% for j = 1 : 15 :length(hip)
% 
%     femur_start = RotMat(hip(j))*[0 0 0].';
%     femur_end = RotMat(hip(j))*[0 -1 0].';
% 
%     shank_start = RotMat(hip(j))*[0 -1 0].';
%     shank_end = RotMat(hip(j))*RotMat(knee(j))*[0 -0.8 0].' + femur_end;
% 
%     foot_start = RotMat(hip(j))*RotMat(knee(j))*[0 -0.8 0].' + femur_end;
%     foot_end = RotMat(hip(j))*RotMat(knee(j))*RotMat(ankle(j))*[0 -0.75 0].' + shank_end;
% 
%     foot_start_2 = RotMat(hip(j))*RotMat(knee(j))*[0 -0.8 0].' + femur_end;
%     foot_end_2 = RotMat(hip(j))*RotMat(knee(j))*RotMat(0)*[0 -0.75 0].' + shank_end;
% 
%     plot([femur_start(1) femur_end(1)],[femur_start(2) femur_end(2)],[femur_start(3) femur_end(3)],'LineWidth',3);
%     hold on;    
%     plot(femur_start(1),femur_start(2),femur_start(3),'.','MarkerSize',75);
%     plot([shank_start(1) shank_end(1)],[shank_start(2) shank_end(2)],[shank_start(3) shank_end(3)],'LineWidth',3);
%     plot(shank_start(1),shank_start(2),shank_start(3),'.','MarkerSize',75);
%     plot([foot_start(1) foot_end(1)],[foot_start(2) foot_end(2)],[foot_start(3) foot_end(3)],'LineWidth',3);
%     plot(foot_start(1),foot_start(2),foot_start(3),'.','MarkerSize',75);
% %     plot([foot_start_2(1) foot_end_2(1)],[foot_start_2(2) foot_end_2(2)],[foot_start_2(3) foot_end_2(3)],':','LineWidth',3);
% %     plot(foot_start_2(1),foot_start_2(2),foot_start_2(3),'.','MarkerSize',75);
%     hold off;
%     grid on;
%     axis([-2.5 2.5 -2.5 2.5]);
%     view([0 90]);
%     drawnow;
%     pause();
% end

%% Plot movement over time

start_pos = 500;
end_pos = 1500;
step = 5;

close all;
h = figure('units','normalized','outerposition',[0 0 1 1]);

subplot_1 = subplot(2,1,1);
hold(subplot_1,'on');
title(subplot_1,'Hind Limb Movment')
xlabel(subplot_1,'X-position [cm]')
ylabel(subplot_1,'Y-position [cm]')
set(subplot_1,'FontSize',20)

subplot_2 = subplot(2,1,2);

count = 1;


for j = start_pos : step : end_pos
    
    plot(subplot_1,[femur_start(1,j) femur_end(1,j)],[femur_start(2,j) femur_end(2,j)],'r',...
        [shank_start(1,j) shank_end(1,j)],[shank_start(2,j) shank_end(2,j)],'g',...
        [foot_start(1,j) foot_end(1,j)],[foot_start(2,j) foot_end(2,j)],'k','LineWidth',3);
    plot(subplot_1,femur_start(1,j),femur_start(2,j),shank_start(1,j),shank_start(2,j),foot_start(1,j),foot_start(2,j),'b.','MarkerSize',30);
    plot(subplot_1,linspace(pos(j,1)-10,pos(j,1)+10,20),ones(1,20)*-2.75,'g','LineWidth',5)
    
    axis(subplot_1,[pos(j,1)-10 pos(j,1)+10 -2.75 1]);

    % Plot muscle activations
    plot(subplot_2,time(j-100:j),muscle_act(j-100:j,:),'LineWidth',2.5);
    hold on
    plot(subplot_2,time(j),muscle_act(j,:),'b.','MarkerSize',20);
    hold off
    axis(subplot_2,[time(j)-0.1 time(j)+0.1 0 1]);
    title(subplot_2,'Hind Limb Muscle Activation')
    xlabel(subplot_2,'Time [s]')
    ylabel(subplot_2,'Activation [0-1]')
    set(subplot_2,'FontSize',20);
    pause(0.1)    
end

end