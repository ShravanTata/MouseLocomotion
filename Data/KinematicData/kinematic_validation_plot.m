
close all;


figure(1)
set(gca,'FontSize',20)
subplot(2,1,1)
plot(result.data(:,1),result.data(:,[5])-50,'LineWidth',2)
hold on
grid on
plot(texp,rad2deg(data_new(:,[1])),':','LineWidth',2)

subplot(2,1,2)
plot(result.data(:,1),result.data(:,[8])-50,'LineWidth',2)
hold on
grid on
plot(texp,rad2deg(data_new(:,[4]`)),':','LineWidth',2)

figure(2)
title('Knee','Fontsize',20)
set(gca,'FontSize',20)
subplot(2,1,1)
plot(result.data(:,1),result.data(:,[6])+150,'LineWidth',2)
hold on
grid on
plot(texp,rad2deg(data_new(:,[2])),':','LineWidth',2)

subplot(2,1,2)
plot(result.data(:,1),result.data(:,[9])+150,'LineWidth',2)
hold on
grid on
plot(texp,rad2deg(data_new(:,[5])),':','LineWidth',2)

figure(3)
title('Ankle','Fontsize',20)
set(gca,'FontSize',20)
subplot(2,1,1)
plot(result.data(:,1),result.data(:,[7])-30,'LineWidth',2)
hold on
grid on
plot(texp,rad2deg(data_new(:,[3])),':','LineWidth',2)

subplot(2,1,2)
plot(result.data(:,1),result.data(:,[10])-30,'LineWidth',2)
hold on
grid on
plot(texp,rad2deg(data_new(:,[6])),':','LineWidth',2)